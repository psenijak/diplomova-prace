/*
  Warnings:

  - A unique constraint covering the columns `[wiki_url]` on the table `City` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[website_url]` on the table `City` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `districtId` to the `City` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "City" ADD COLUMN     "districtId" INTEGER NOT NULL;

-- CreateTable
CREATE TABLE "Region" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "wiki_url" TEXT NOT NULL,
    "countryId" INTEGER NOT NULL,

    CONSTRAINT "Region_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "District" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "wiki_url" TEXT NOT NULL,
    "regionId" INTEGER NOT NULL,

    CONSTRAINT "District_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Region_wiki_url_key" ON "Region"("wiki_url");

-- CreateIndex
CREATE UNIQUE INDEX "District_wiki_url_key" ON "District"("wiki_url");

-- CreateIndex
CREATE UNIQUE INDEX "City_wiki_url_key" ON "City"("wiki_url");

-- CreateIndex
CREATE UNIQUE INDEX "City_website_url_key" ON "City"("website_url");

-- AddForeignKey
ALTER TABLE "District" ADD CONSTRAINT "District_regionId_fkey" FOREIGN KEY ("regionId") REFERENCES "Region"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "City" ADD CONSTRAINT "City_districtId_fkey" FOREIGN KEY ("districtId") REFERENCES "District"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
