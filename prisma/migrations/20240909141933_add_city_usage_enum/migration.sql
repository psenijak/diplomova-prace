/*
  Warnings:

  - You are about to drop the column `isSecured` on the `City` table. All the data in the column will be lost.

*/
-- CreateEnum
CREATE TYPE "UsageType" AS ENUM ('manual', 'training', 'testing');

-- AlterTable
ALTER TABLE "City" DROP COLUMN "isSecured",
ADD COLUMN     "usageType" "UsageType"[];
