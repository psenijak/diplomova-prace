/*
  Warnings:

  - You are about to drop the column `WCAGComplianceProblems` on the `City` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "City" DROP COLUMN "WCAGComplianceProblems",
ADD COLUMN     "WCAGCriticalProblemCount" INTEGER,
ADD COLUMN     "WCAGMinorProblemCount" INTEGER,
ADD COLUMN     "WCAGModerateProblemCount" INTEGER,
ADD COLUMN     "WCAGSeriousProblemCount" INTEGER;
