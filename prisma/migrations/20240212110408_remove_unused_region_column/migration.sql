/*
  Warnings:

  - You are about to drop the column `countryId` on the `Region` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Region" DROP COLUMN "countryId";
