/*
  Warnings:

  - You are about to drop the column `hasSEOOptimization` on the `City` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "City" DROP COLUMN "hasSEOOptimization",
ADD COLUMN     "seoScore" INTEGER;
