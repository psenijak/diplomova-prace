-- DropForeignKey
ALTER TABLE "City" DROP CONSTRAINT "City_districtId_fkey";

-- AlterTable
ALTER TABLE "City" ALTER COLUMN "districtId" DROP NOT NULL;

-- AddForeignKey
ALTER TABLE "City" ADD CONSTRAINT "City_districtId_fkey" FOREIGN KEY ("districtId") REFERENCES "District"("id") ON DELETE SET NULL ON UPDATE CASCADE;
