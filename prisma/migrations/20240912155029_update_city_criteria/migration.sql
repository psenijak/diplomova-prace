/*
  Warnings:

  - You are about to drop the column `ContrastProblems` on the `City` table. All the data in the column will be lost.
  - You are about to drop the column `hasSiteMapAndRobots` on the `City` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "City" DROP COLUMN "ContrastProblems",
DROP COLUMN "hasSiteMapAndRobots",
ADD COLUMN     "contrastProblems" INTEGER,
ADD COLUMN     "hasEmailsOnMainPage" BOOLEAN,
ADD COLUMN     "hasPhoneNumbersOnMainPage" BOOLEAN,
ADD COLUMN     "hasRobots" BOOLEAN,
ADD COLUMN     "hasSiteMap" BOOLEAN;
