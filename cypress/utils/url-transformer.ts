/**
 * Transforms a given URL to use HTTPS if it originally uses HTTP.
 *
 * @param {string} url - The URL to transform.
 * @returns {string} - The transformed URL with HTTPS if applicable, otherwise returns the original URL.
 *
 * This function checks if the URL starts with 'http://', and if so, replaces it with 'https://'
 * to ensure the URL is secured. If the URL is already secured or does not use HTTP, it is returned unchanged.
 */
export const transformToSecuredUrl = (url: string): string => {
  if (url.startsWith('http://')) {
    return url.replace('http://', 'https://');
  }
  return url;
};

/**
 * Transforms a given URL to use HTTP if it originally uses HTTPS.
 *
 * @param {string} url - The URL to transform.
 * @returns {string} - The transformed URL with HTTP if applicable, otherwise returns the original URL.
 *
 * This function checks if the URL starts with 'https://', and if so, replaces it with 'http://'
 * to convert the URL to an unsecured version. If the URL is already unsecured or does not use HTTPS, it is returned unchanged.
 */
export const transformToUnsecuredUrl = (url: string): string => {
  if (url.startsWith('https://')) {
    return url.replace('https://', 'http://');
  }
  return url;
};
