import { City } from '@prisma/client';
import { checkSecurityOfPage } from '../support/check-security-of-page';
import 'cypress-axe';
import { checkContentOnPages } from '../support/content-page-loop';
import { gatherContactLinksOnPage } from '../support/find-contact-links';
import { ensureMinimumContactLinks } from '../support/get-additional-page-links';
import { getElementsWithEmails } from '../support/get-element-list-of-email-adress';
import { getElementsWithPhoneNumbers } from '../support/get-element-list-of-mobile-numbers';
import { validateContactLinks } from '../support/validate-contact-links';

/**
 * Cypress test suite for visiting city websites and analyzing their quality.
 *
 * This suite performs a series of tests on various city websites to assess their quality
 * and verify specific characteristics. The URLs of the city websites can be specified
 * through comments or by setting the `baseUrl` environment variable.
 *
 * The suite uses Cypress tasks to fetch the city details from a database before
 * each test is run, ensuring the city exists. Each test begins with navigating to the specified URL.
 *
 * Usage:
 * - Suitable for automated website quality assessments across different municipalities.
 * - Before running the tests, ensure the city's URL is present in the database.
 *
 * Note:
 * - The `before` hook checks for the existence of the city, while `beforeEach` navigates to the website URL.
 */
describe("Visit each city website and analyze it's quality", () => {
  // const url = "https://www.obecblazejovice.cz/"; // nefunkční
  // const url = 'http://www.praha.eu/';
  // const url = "https://www.obec-ctibor.cz/";
  // const url = 'http://www.bernartice-borovsko.cz/';
  // const url = 'http://www.hradeckralove.org/';

  // const url = 'http://www.bilkovice.cz/';
  // const url = 'http://www.obec-borovnice.cz/';
  // const url = 'http://www.mestobystrice.cz/';
  // const url = 'http://www.benesov-city.cz/';
  // const url = 'http://www.mukolin.cz/';
  // const url = 'http://www.meucaslav.cz/';
  // const url = 'http://www.kutnahora.cz/';
  // const url = 'http://www.melnik.cz/';
  // const url = 'http://www.mesto-nymburk.cz/';
  // const url = 'http://www.predboj.cz/';
  // const url = 'http://www.mestobustehrad.cz/';
  // const url = 'http://www.libcice.cz/';
  // const url = 'http://www.obec-bukovany.cz/';
  // const url = 'http://www.troubsko.cz';
  // const url = 'http://www.chotebor.cz/';
  // const url = 'http://www.libenice.cz/';
  // const url = 'https://www.mesto-kunovice.cz';
  // const url = 'http://www.kovanice.cz/';
  // const url = 'https://www.ledceps.cz/';
  // const url = 'http://www.skalsko.cz/';
  // const url = 'http://www.ricany.cz/';

  // google address cause end of the tests because it is not in database
  const url = Cypress.env('baseUrl') || 'http://www.google.com/';

  // };
  let city: City = null;

  before(() => {
    cy.task('findCityByUrl', url)
      .then((result: City) => {
        city = result;
      })
      .then(() => {
        if (!city) {
          assert.fail('City not found');
        }
      });
  });

  beforeEach(() => {
    cy.visit(url);
  });

  /**
   * Test to verify if the city website uses a secure protocol (HTTPS).
   *
   * This test checks whether the specified city website is served over HTTPS.
   * The result is stored in the `isPageSecure` alias, and the city's database record
   * is updated to reflect whether the website uses HTTPS.
   *
   * Usage:
   * - Ensures that the city website follows best practices for secure connections.
   * - The test outcome is used to update the city's security status in the database.
   */
  it('should have secure protocol - https', () => {
    checkSecurityOfPage(url);
    cy.get('@isPageSecure').then((isSecure) => {
      cy.task('updateCity', {
        id: city.id,
        city: { hasHTTPS: isSecure },
      });
    });
  });

  /**
   * Test to verify if the city website uses semantic links for mobile numbers.
   *
   * This test searches the webpage for elements that contain phone numbers and checks
   * if they are properly formatted as semantic links (e.g., <a href="tel:...">). The city's
   * database record is updated to reflect whether the website correctly uses semantic links.
   *
   * Usage:
   * - Ensures that phone number links on the city website follow best practices for semantic markup.
   * - Updates the city's database entry based on the presence of correctly formatted 'tel:' links.
   */
  it('should have semantic links for mobile numbers', () => {
    getElementsWithPhoneNumbers();
    cy.get('@mobileNumberElementList').then((matchedElements) => {
      const elements =
        matchedElements && typeof matchedElements.toArray === 'function'
          ? (matchedElements.toArray() as HTMLElement[])
          : ([] as HTMLElement[]);

      elements.forEach((el: HTMLElement) => {
        if (el.tagName === 'A' && el.hasAttribute('href')) {
          const href = el.getAttribute('href');
          if (href && href.startsWith('tel:')) {
            city.hasCorrectSemanticLinks = true;
            cy.log(
              `Odkaz v elementu ${el.tagName} má správně formátovaný odkaz: ${href}`,
            );
          } else {
            city.hasCorrectSemanticLinks = false;
            cy.log(
              `Odkaz v elementu ${el.tagName} nemá správně formátovaný odkaz: ${href}`,
            );
          }
        } else {
          cy.log(`Element ${el.tagName} není odkaz nebo nemá atribut 'href'.`);
        }
      });
      cy.task('updateCity', {
        id: city.id,
        city: { hasCorrectSemanticLinks: city.hasCorrectSemanticLinks },
      });
    });
  });

  /**
   * Test to verify if the city website's main page contains mobile phone numbers.
   *
   * This test searches for elements on the main page that contain phone numbers. It logs the number
   * of elements found and updates the city's database record to indicate whether mobile numbers
   * are present on the main page.
   *
   * Usage:
   * - Helps ensure that contact information is easily accessible on the city's website.
   * - The test outcome is used to update the city's record on the presence of phone numbers.
   */
  it('should have mobile number on main page', () => {
    getElementsWithPhoneNumbers();
    cy.get('@mobileNumberElementList').then((matchedElements) => {
      const elements =
        matchedElements && typeof matchedElements.toArray === 'function'
          ? (matchedElements.toArray() as HTMLElement[])
          : ([] as HTMLElement[]);

      if (elements.length === 0) {
        cy.log('Na stránce nebyl nalezen žádný element s telefonním číslem.');
      } else {
        cy.log(
          `Na stránce bylo nalezeno ${elements.length} elementů s telefonním číslem.`,
        );
      }
      cy.task('updateCity', {
        id: city.id,
        city: { hasPhoneNumbersOnMainPage: elements.length > 0 },
      });
    });
  });

  /**
   * Test to verify if the city website's main page contains email addresses.
   *
   * This test searches for elements on the main page that contain email addresses. It logs the number
   * of elements found and updates the city's database record to indicate whether email addresses
   * are present on the main page.
   *
   * Usage:
   * - Ensures that contact information, specifically email addresses, is accessible on the city's website.
   * - The test outcome is used to update the city's record on the presence of email addresses.
   */
  it('should have email on main page', () => {
    getElementsWithEmails();
    cy.get('@emailsElementList').then((matchedElements) => {
      const elements =
        matchedElements && typeof matchedElements.toArray === 'function'
          ? (matchedElements.toArray() as HTMLElement[])
          : ([] as HTMLElement[]);

      if (elements.length === 0) {
        cy.log('Na stránce nebyl nalezen žádný element s emailovou adresou.');
      } else {
        cy.log(
          `Na stránce bylo nalezeno ${elements.length} elementů s emailovou adresou.`,
        );
      }
      cy.task('updateCity', {
        id: city.id,
        city: { hasEmailsOnMainPage: elements.length > 0 },
      });
    });
  });

  /**
   * Test to verify if the city website uses semantic links for email addresses.
   *
   * This test searches for elements containing email addresses on the webpage and checks
   * if they are properly formatted as semantic links (e.g., <a href="mailto:...">). The result
   * is used to update the city's database record to indicate whether the website correctly
   * utilizes semantic links for emails.
   *
   * Usage:
   * - Ensures that email links on the city's website follow best practices for semantic markup.
   * - The test outcome is used to update the city's record on the correct usage of 'mailto:' links.
   */
  it('should have semantic links for emails', () => {
    getElementsWithEmails();
    cy.get('@emailsElementList').then((matchedElements) => {
      const elements =
        matchedElements && typeof matchedElements.toArray === 'function'
          ? (matchedElements.toArray() as HTMLElement[])
          : ([] as HTMLElement[]);

      elements.forEach((el: HTMLElement) => {
        debugger;
        if (el.tagName === 'A' && el.hasAttribute('href')) {
          const href = el.getAttribute('href');
          if (href && href.startsWith('mailto:')) {
            if (city.hasCorrectSemanticLinks !== false) {
              city.hasCorrectSemanticLinks = true;
            }
            cy.log(
              `Odkaz v elementu ${el.tagName} má správně formátovaný odkaz: ${href}`,
            );
          } else {
            city.hasCorrectSemanticLinks = false;
            cy.log(
              `Odkaz v elementu ${el.tagName} nemá správně formátovaný odkaz: ${href}`,
            );
          }
        } else {
          cy.log(`Element ${el.tagName} není odkaz nebo nemá atribut 'href'.`);
        }
      });
      cy.task('updateCity', {
        id: city.id,
        city: { hasCorrectSemanticLinks: city.hasCorrectSemanticLinks },
      });
    });
  });

  /**
   * Test to verify that each image on the city website has a non-empty alt attribute.
   *
   * This test checks all images on the webpage to ensure they have a defined and non-empty
   * `alt` attribute, which is important for accessibility. If no images are found, the result
   * is set to null. The outcome is used to update the city's database record regarding the presence
   * of alt attributes on images.
   *
   * Usage:
   * - Ensures that images on the city website follow accessibility best practices by using alt attributes.
   * - The test outcome is stored in the city's record to indicate whether alt attributes are present.
   */
  it('should ensure each image has a non-empty alt attribute', () => {
    let hasAltAttributes = null; // Start with null to indicate no images processed

    cy.get('img')
      .then(($imgs) => {
        if ($imgs.length === 0) {
          cy.log('No images found on the page.');
          hasAltAttributes = null;
        } else {
          hasAltAttributes = true;
          $imgs.each(function () {
            const $img = Cypress.$(this);
            const altAttribute = $img.attr('alt');

            if (altAttribute === undefined) {
              cy.log(
                `Image with src='${$img.attr('src')}' has no alt attribute.`,
              );
              hasAltAttributes = false;
            } else if (altAttribute === '') {
              cy.log(
                `Image with src='${$img.attr('src')}' has an empty alt attribute.`,
              );
              hasAltAttributes = false;
            } else {
              cy.log(
                `Image with src='${$img.attr('src')}' has a non-empty alt attribute: ${altAttribute}`,
              );
            }
          });
        }
      })
      .then(() => {
        cy.log(`Result of alt attribute validation: ${hasAltAttributes}`);
        cy.task('updateCity', {
          id: city.id,
          city: { hasALTAttributes: hasAltAttributes },
        });
      });
  });

  /**
   * Test to verify the presence and reachability of the website's sitemap.xml file.
   *
   * This test attempts to request the /sitemap.xml file and checks if the response
   * status is 200 and the content type is 'application/xml'. The result is used to
   * update the city's database record to indicate whether the sitemap is accessible
   * and correctly formatted.
   *
   * Usage:
   * - Ensures that the city website provides a sitemap in XML format, which is important
   *   for SEO and web crawlers.
   * - The test result updates the city's record on the presence and accessibility of the sitemap.
   */
  it('should have reachable sitemap.xml', () => {
    cy.task('updateCity', {
      id: city.id,
      city: { hasSiteMap: false },
    });

    cy.request({
      url: '/sitemap.xml',
      failOnStatusCode: false,
      timeout: 10000,
    }).then((response) => {
      const hasSiteMap =
        response.status === 200 &&
        response.headers['content-type'].includes('application/xml');
      if (hasSiteMap) {
        cy.log('Sitemap is in XML format');
      } else {
        cy.log('Sitemap is not reachable or not in XML format');
      }
      cy.task('updateCity', {
        id: city.id,
        city: { hasSiteMap },
      });
    });
  });

  /**
   * Test to verify the presence and validity of the website's robots.txt file.
   *
   * This test requests the /robots.txt file and checks if the response status is 200,
   * and whether the file contains the expected directives such as 'User-agent: *' and 'Disallow:'.
   * The result is used to update the city's database record to indicate whether the robots.txt file
   * is accessible and properly formatted.
   *
   * Usage:
   * - Ensures that the city website provides a valid robots.txt file, which is important for SEO
   *   and guiding web crawlers.
   * - The test outcome updates the city's record on the presence and validity of the robots.txt file.
   */
  it('should have reachable robots.txt', () => {
    // Check robots.txt
    cy.request({
      url: '/robots.txt',
      failOnStatusCode: false,
    }).then((response) => {
      const hasRobots =
        response.status === 200 &&
        response.body.includes('User-agent: *') &&
        response.body.includes('Disallow:');
      if (hasRobots) {
        cy.log('Robots.txt is valid');
      } else {
        cy.log('Robots.txt is not valid');
      }
      cy.task('updateCity', {
        id: city.id,
        city: { hasRobots },
      });
    });
  });

  /**
   * Test to verify the website's compliance with WCAG accessibility guidelines.
   *
   * This test uses the Axe accessibility testing library to check for WCAG 2.0 Level A and AA
   * violations on the website. It runs accessibility checks with a timeout of 60 seconds and
   * allows up to 2 retries. The detected violations are logged and stored in a JSON report.
   *
   * Usage:
   * - Ensures the city website follows accessibility standards for better usability.
   * - The results are stored in a JSON file named 'a11y-report.json' for further analysis.
   *
   * Note:
   * - The test is configured with extended timeouts and retries to accommodate potentially long checks.
   */
  it(
    'should have valid wcag guidelines',
    { requestTimeout: 60000, retries: 2, defaultCommandTimeout: 60000 },
    () => {
      cy.injectAxe();
      cy.checkA11y(
        null,
        {
          runOnly: {
            type: 'tag',
            values: ['wcag2a', 'wcag2aa'],
          },
          retries: 2,
        },
        (violations) => {
          cy.task('writeReport', {
            filename: `data/a11y-report.json`,
            content: violations,
          });
        },
        true,
      );
    },
  );

  /**
   * Test to verify the website's compliance with color contrast accessibility guidelines.
   *
   * This test uses the Axe accessibility testing library to specifically check for color contrast
   * issues on the website. It runs the color contrast rule with a timeout of 60 seconds and allows
   * up to 2 retries. Any detected violations are logged and stored in a JSON report.
   *
   * Usage:
   * - Focuses on ensuring the city website meets color contrast accessibility requirements for better readability.
   * - The results are stored in a JSON file named 'a11y-report-color-contrast.json' for further analysis.
   *
   * Note:
   * - The test is configured with extended timeouts and retries to accommodate potentially long checks.
   */
  it(
    'should test only color contrast',
    { requestTimeout: 60000, retries: 2, defaultCommandTimeout: 60000 },
    () => {
      cy.injectAxe();
      cy.checkA11y(
        null,
        {
          runOnly: {
            type: 'rule',
            values: ['color-contrast'], // Specifies only the color contrast rule to run
          },
        },
        (violations) => {
          cy.task('writeReport', {
            filename: `data/a11y-report-color-contrast.json`,
            content: violations,
          });
        },
        true,
      );
    },
  );

  /**
   * Test to verify the website's compliance with GDPR cookie consent requirements.
   *
   * This test checks if any Google Analytics cookies (_ga, _gid, _gat) are set before explicit user consent
   * is given. The website is visited after clearing all cookies, and the presence of Google Analytics cookies
   * is logged. The city's database record is updated to indicate whether the website respects cookies consent.
   *
   * Usage:
   * - Ensures that the city website complies with GDPR regulations regarding cookie consent.
   * - The test outcome updates the city's record on whether cookies are set without user consent.
   */
  it('GDPR Compliance Test', () => {
    cy.clearCookies();
    cy.visit(url);

    cy.wait(1000);

    cy.getCookies().then((cookies) => {
      const gaCookies = cookies.filter((cookie) =>
        cookie.name.match(/^_ga|^_gid|^_gat/),
      );

      if (gaCookies.length > 0) {
        gaCookies.forEach((cookie) => {
          cy.log(
            `Google Analytics cookie found: ${cookie.name} value: ${cookie.value}`,
          );
        });
      } else {
        cy.log('No Google Analytics cookies found.');
      }
      city.respectsCookiesConsent = gaCookies.length === 0;
      cy.task('updateCity', {
        id: city.id,
        city: { respectsCookiesConsent: city.respectsCookiesConsent },
      });
    });
  });

  /**
   * Test to verify if the website respects the "Do Not Track" (DNT) browser setting.
   *
   * This test sets the browser's DNT flag to "1" and monitors network requests to Google Analytics.
   * If a request is made to Google Analytics despite the DNT setting, it indicates non-compliance.
   * The result is logged, and the city's database record is updated based on whether the website
   * respects the cookies consent setting.
   *
   * Usage:
   * - Ensures that the city website honors the DNT setting and does not send data to analytics services.
   * - Helps in verifying the city's compliance with user privacy preferences.
   */
  it('respects the DNT setting', () => {
    let gaRequestMade = false;

    cy.intercept('https://www.google-analytics.com/**', (req) => {
      gaRequestMade = true;
    }).as('gaRequest');

    cy.visit(url, {
      onBeforeLoad: (win) => {
        Object.defineProperty(win.navigator, 'doNotTrack', { value: '1' });
      },
    });

    cy.wait(3000).then(() => {
      if (gaRequestMade) {
        cy.log('Data were sent to GA.');
      } else {
        cy.log('The website is in compliance; no data were sent to GA.');
      }
      if (city.respectsCookiesConsent) {
        cy.task('updateCity', {
          id: city.id,
          city: { respectsCookiesConsent: city.respectsCookiesConsent },
        });
      }
    });
  });

  /**
   * Test to count and log unique social media links present on the city website.
   *
   * This test checks for the presence of social media links from various popular platforms
   * (e.g., Facebook, Instagram, Twitter, LinkedIn, YouTube, Pinterest, TikTok) on the webpage.
   * It counts the number of unique platforms found and logs the results. The city's database
   * record is updated to indicate whether the website contains links to social networks.
   *
   * Usage:
   * - Ensures that the city website provides links to social media accounts for better user engagement.
   * - The test outcome updates the city's record on the presence of social media links.
   */
  it('should count unique social media links on the page', () => {
    const socialNetworks = [
      'facebook.com',
      'instagram.com',
      'twitter.com',
      'linkedin.com',
      'youtube.com',
      'pinterest.com',
      'tiktok.com',
    ];

    const foundNetworks = new Set();

    socialNetworks.forEach((network) => {
      cy.get('body').then(($body) => {
        const link = $body.find(`a[href*="${network}"]`);
        if (link.length > 0) {
          foundNetworks.add(network);
        }
      });
    });

    cy.then(() => {
      cy.log(
        `Number of unique social media links found: ${foundNetworks.size}`,
      );
      if (foundNetworks.size > 0) {
        cy.log('Found networks: ' + Array.from(foundNetworks).join(', '));
      } else {
        cy.log('No social media links found.');
      }
      cy.task('updateCity', {
        id: city.id,
        city: { hasSocialNetworks: foundNetworks.size > 0 },
      });
    });
  });

  /**
   * Test to verify the correct hierarchy of heading levels (h1, h2, h3, etc.) on the city website.
   *
   * This test examines all heading elements on the page to ensure that they follow a proper hierarchical
   * order without skipping levels (e.g., h2 follows h1, h3 follows h2). It also checks if the first heading
   * used is an h1. Any discrepancies in the heading order are logged, and the city's database record is updated
   * to indicate whether the website has a correct hierarchy of headings.
   *
   * Usage:
   * - Ensures that the city website maintains a proper heading structure for accessibility and SEO.
   * - The test outcome updates the city's record on the presence of a correct heading hierarchy.
   */
  it('checks for correct hierarchy of headings on the page', () => {
    cy.get('h1, h2, h3, h4, h5, h6')
      .then((headings) => {
        const foundLevels: number[] = [];

        headings.each((index, heading) => {
          const level = parseInt(heading.tagName.substring(1), 10) as number;
          if (!foundLevels.includes(level)) {
            foundLevels.push(level);
          }
        });
        city.hasCorrectHierarchyOfHeadings = true;

        const sortedLevels = foundLevels.sort((a, b) => a - b);

        for (let i = 1; i < sortedLevels.length; i++) {
          if (sortedLevels[i] !== sortedLevels[i - 1] + 1) {
            cy.log(
              `Heading level h${sortedLevels[i]} follows h${sortedLevels[i - 1]} without an intermediate heading.`,
            );
            city.hasCorrectHierarchyOfHeadings = false;
          }
        }

        if (sortedLevels[0] !== 1) {
          cy.log('h1 is missing or not the first heading level used.');
          city.hasCorrectHierarchyOfHeadings = false;
        }
      })
      .then(() => {
        cy.task('updateCity', {
          id: city.id,
          city: {
            hasCorrectHierarchyOfHeadings: city.hasCorrectHierarchyOfHeadings,
          },
        });
      });
  });

  /**
   * Test to identify and validate potential contact page links on the city website.
   *
   * This test first gathers initial contact links on the main page and then validates them.
   * If contact links are found, it recursively searches for additional nested contact links
   * by visiting the identified links. The process is repeated to gather and validate all unique
   * contact links, which are then updated in the city's database record.
   *
   * Usage:
   * - Helps identify pages that provide contact information on the city's website.
   * - The test outcome updates the city's record with a list of validated contact links (up to 30).
   */
  it('identifies potential contact page links', () => {
    gatherContactLinksOnPage(url).then((initialLinks) => {
      cy.log(`Initial contact links found: ${initialLinks.join(', ')}`);

      if (initialLinks.length === 0) {
        cy.log('No contact links found on the main page.');
        return;
      }

      const allContactLinks = new Set(initialLinks);

      validateContactLinks(initialLinks).then((validLinks) => {
        Cypress.Promise.each(validLinks, (link) => {
          return cy.visit(link).then(() => {
            return gatherContactLinksOnPage(url).then((nestedLinks) => {
              cy.log(
                `Nested contact links found from ${link}: ${nestedLinks.join(
                  ', ',
                )}`,
              );
              nestedLinks.forEach((nestedLink) => {
                allContactLinks.add(nestedLink);
              });
            });
          });
        }).then(() => {
          const uniqueContactLinks = Array.from(allContactLinks);
          cy.log(
            `Unique contact links gathered: ${uniqueContactLinks.join(', ')}`,
          );

          validateContactLinks(uniqueContactLinks).then((validLinks) => {
            cy.log(`Valid contact links: ${validLinks.join(', ')}`);

            cy.task('updateCity', {
              id: city.id,
              city: { contactLinks: validLinks?.slice(0, 30) },
            });
          });
        });
      });
    });
  });

  /**
   * Test to verify that cookies on the city website are set with secure attributes.
   *
   * This test checks if all cookies found on the website have the "Secure" and "HttpOnly" attributes set,
   * and that the "SameSite" attribute is set to either "Strict" or "Lax". If any cookie does not meet these
   * security requirements, it is logged as insecure. The city's database record is updated to indicate
   * whether all cookies are secure.
   *
   * Usage:
   * - Ensures the city website follows best practices for cookie security to protect user data.
   * - The test outcome updates the city's record on the security of its cookie settings.
   */
  it('should have secure attributes set on cookies', () => {
    cy.log('found city population: ', JSON.stringify(city, null, 4));
    cy.getCookies().then((cookies) => {
      if (cookies.length === 0) {
        cy.log('No cookies found on the site.');
      } else {
        let allCookiesSecure = true;
        cookies.forEach((cookie) => {
          const secure = cookie.secure;
          const httpOnly = cookie.httpOnly;
          const sameSite = ['strict', 'lax'].includes(cookie.sameSite);

          if (!secure || !httpOnly || !sameSite) {
            allCookiesSecure = false;
            cy.log(
              `Cookie ${cookie.name} is not secure. Secure: ${secure}, HttpOnly: ${httpOnly}, SameSite: ${cookie.sameSite}`,
            );
          }
        });

        if (allCookiesSecure) {
          cy.log('All cookies are secure.');
        } else {
          cy.log('Some cookies are not set securely.');
        }
        cy.task('updateCity', {
          id: city.id,
          city: { hasSecuredCookies: allCookiesSecure },
        });
      }
    });
  });

  /**
   * Test to verify the presence of a link or text referring to the official board on the city website.
   *
   * This test searches the webpage for a link or text element that matches the term "úřední deska"
   * (official board). If such a link or text is found, it is logged, and the corresponding element
   * is scrolled into view for debugging. The city's database record is updated to indicate whether
   * a reference to the official board was found.
   *
   * Usage:
   * - Ensures the city website provides easy access to information about the official board.
   * - The test outcome updates the city's record on the presence of an official board link or text.
   */
  it('should find a link or text referring to the official board', () => {
    let found = false;
    cy.get('body')
      .then(($body) => {
        const normalizedText = 'úřední deska';
        const links = $body.find('a').filter((i, el) => {
          return Cypress.$(el).text().toLowerCase().includes(normalizedText);
        });

        if (links.length > 0) {
          cy.wrap(links)
            .first()
            .then(($link) => {
              cy.log(
                `Found a link to the official board: ${$link.attr('href')}`,
              );

              found = true;
              cy.wrap($link).scrollIntoView().debug();
            });
        } else {
          const textOccurrences = $body
            .find('div, p, li, span')
            .filter((i, el) => {
              return (
                Cypress.$(el).text().toLowerCase().trim() === normalizedText
              );
            });
          if (textOccurrences.length > 0) {
            cy.wrap(textOccurrences)
              .first()
              .then(($textOccurrence) => {
                cy.log(
                  'Found specific text occurrences referring to the official board',
                );

                found = true;
                cy.wrap($textOccurrence).scrollIntoView().debug();
              });
          } else {
            found = false;
            cy.log(
              'No specific text or link referring to the official board was found on the page',
            );
          }
        }
      })
      .then(() => {
        cy.task('updateCity', {
          id: city.id,
          city: { hasOfficialBoard: found },
        });
      });
  });

  /**
   * Test to verify the presence of working hours information on the city website.
   *
   * This test searches the main and contact pages for keywords related to working hours (e.g., "úřední hodiny",
   * "otevírací doba") and checks for time patterns indicating working hours. If found, it updates the city's
   * database record to indicate the presence of working hours information.
   *
   * Usage:
   * - Ensures the city website provides accessible information about working hours for public services.
   * - The test outcome updates the city's record on the presence of working hours information.
   */
  it('should find working hours through main and contact pages', () => {
    checkContentOnPages(city.contactLinks, () => {
      return cy.get('body').then(($body) => {
        const keywords = [
          'úřední hodiny',
          'hodiny pro veřejnost',
          'otevírací doba',
          'Úřední hodiny',
          'Úřední dny',
          'Úřední doba',
        ];
        const timePattern =
          /(\d{1,2}:\d{2}\s*[–-]\s*\d{1,2}:\d{2})|(\d{1,2}–\d{2}\s*a\s*\d{1,2}–\d{2})/;
        let found = false;
        $body.find('div, p, dl, dt, td, span, h4').each((i, el) => {
          const text = Cypress.$(el).text().toLowerCase();
          if (keywords.some((keyword) => text.includes(keyword))) {
            const $parent = Cypress.$(el).parent();
            const $elementsToCheck = $parent.find(
              'div, p, dl, dt, td, span, h4, li',
            );
            $elementsToCheck.each((j, subEl) => {
              const subText = Cypress.$(subEl).text();
              if (timePattern.test(subText)) {
                found = true;
                return false;
              }
            });
            if (found) return false;
          }
        });
        return cy.wrap(found);
      });
    }).then((result) => {
      if (result) {
        cy.log('Test Passed: Working hours were found.');
      } else {
        cy.log('Test Failed: No working hours were found.');
      }
      cy.task('updateCity', {
        id: city.id,
        city: { hasWorkingHours: result },
      });
    });
  });

  /**
   * Test to verify the presence of inquiry forms on the city website.
   *
   * This test searches the main and contact pages for form elements that match typical inquiry form
   * characteristics, such as having an email field, a message field, or specific contact-related attributes.
   * If such a form is found, the city's database record is updated to indicate the presence of a contact form.
   *
   * Usage:
   * - Ensures the city website provides an accessible inquiry form for public communication.
   * - The test outcome updates the city's record on the presence of a contact form.
   */
  it('should find inquiry forms based on form structure and field names', () => {
    checkContentOnPages(city.contactLinks, () => {
      return cy.get('body').then(($body) => {
        let found = false;
        const contactKeywords = [/contact/i, /kontakt/i];

        $body.find('form').each((i, form) => {
          const $form = Cypress.$(form);
          const actionAttribute = $form.attr('action') || '';
          const formAttributes =
            ($form.attr('name') || '') + ' ' + ($form.attr('onSubmit') || '');
          const inputs = $form.find('input');
          const textareas = $form.find('textarea');
          const emailInput = inputs.filter((i, input) => {
            const type = Cypress.$(input).attr('type');
            const name = Cypress.$(input).attr('name');
            return (
              type === 'email' || (name && name.toLowerCase().includes('email'))
            );
          });
          const messageInput =
            textareas.length > 0 ||
            inputs.filter((i, input) => {
              const name = Cypress.$(input).attr('name');
              return name && name.toLowerCase().includes('message');
            }).length > 0;

          if (emailInput.length > 0 && messageInput) {
            found = true;
            cy.wrap($form);
            return false;
          }

          if (
            contactKeywords.some((keyword) => keyword.test(actionAttribute)) &&
            inputs.length >= 2
          ) {
            found = true;
            cy.wrap($form);
            return false;
          }

          if (
            contactKeywords.some((keyword) => keyword.test(formAttributes)) &&
            emailInput.length > 0 &&
            inputs.length >= 2
          ) {
            found = true;
            cy.wrap($form);
            return false;
          }
        });

        return cy.wrap(found);
      });
    }).then((result) => {
      cy.task('updateCity', {
        id: city.id,
        city: { hasContactForm: result },
      });
      if (result) {
        cy.log('Test Passed: Inquiry form was found.');
      } else {
        cy.log('Test Failed: No inquiry form was found.');
      }
    });
  });

  /**
   * Test to verify the presence of breadcrumbs on the city website.
   *
   * This test navigates through the main and contact pages, searching for elements
   * that match typical breadcrumb characteristics based on keywords, attributes, and
   * structure. If a breadcrumb-like structure with links is found within a specified
   * child depth, the city's database record is updated to indicate the presence of breadcrumbs.
   *
   * Usage:
   * - Ensures the city website provides navigational breadcrumbs to improve user experience.
   * - The test outcome updates the city's record on the presence of breadcrumbs.
   */
  it('should find breadcrumbs on the page', () => {
    ensureMinimumContactLinks(url, city.contactLinks)
      .then((contactLinks) => {
        validateContactLinks(contactLinks).then((validLinks) => {
          checkContentOnPages(validLinks, () => {
            return cy.get('body').then(($body) => {
              const keywords = [
                'breadcrumb',
                'cesta',
                'umístění',
                'kontext',
                'navigace',
                'drobečková navigace',
              ];
              const maxChildDepth = 6;
              let found = false;

              const getChildDepth = (element) => {
                let maxDepth = 0;

                const checkDepth = (el, depth) => {
                  if (depth > maxDepth) {
                    maxDepth = depth;
                  }
                  Cypress.$(el)
                    .children()
                    .each((i, child) => {
                      checkDepth(child, depth + 1);
                    });
                };

                checkDepth(element, 1);
                return maxDepth;
              };

              const matchesKeywordInText = (text, keywords) => {
                return keywords.some((keyword) => {
                  const regex = new RegExp(`\\b${keyword}\\b`, 'i');
                  return regex.test(text);
                });
              };

              $body.find('*').each((i, el) => {
                const $el = Cypress.$(el);
                const className = ($el.attr('class') || '').toLowerCase();
                const ariaLabel = ($el.attr('aria-label') || '').toLowerCase();
                const text = $el.text().toLowerCase().trim();

                const matchesKeywordInAttributes =
                  keywords.some((keyword) => className.includes(keyword)) ||
                  keywords.some((keyword) => ariaLabel.includes(keyword));

                if (
                  matchesKeywordInAttributes ||
                  matchesKeywordInText(text, keywords)
                ) {
                  const $parent = $el.closest('nav, ol, div, p');

                  if (
                    $parent.length &&
                    getChildDepth($parent[0]) <= maxChildDepth
                  ) {
                    const links = $parent.find('a');

                    if (links.length > 0) {
                      cy.wrap($parent).scrollIntoView();
                      found = true;
                      return false;
                    }
                  }
                }
              });

              return cy.wrap(found);
            });
          });
        });
      })
      .then((result) => {
        cy.task('updateCity', {
          id: city.id,
          city: { hasBreadcrumbs: result },
        });
        if (result) {
          cy.log('Test Passed: Breadcrumbs were found.');
        } else {
          cy.log('Test Failed: No breadcrumbs were found.');
        }
      });
  });

  /**
   * Test to verify the presence of a calendar or events section on the city website.
   *
   * This test scans the main and contact pages for elements that suggest a calendar or
   * event listing, based on common keywords, month names, and numerical sequences.
   * If a matching element or structure is found, the city's database record is updated
   * to reflect the presence of an event calendar.
   *
   * Usage:
   * - Ensures the city website provides an accessible events or calendar section.
   * - The test outcome updates the city's record on the presence of a calendar.
   */
  it('should find calendar on the page', () => {
    ensureMinimumContactLinks(url, city.contactLinks)
      .then((contactLinks) => {
        validateContactLinks(contactLinks).then((validLinks) => {
          checkContentOnPages(validLinks, () => {
            return cy.get('body').then(($body) => {
              const calendarKeywords = [
                'kalendář',
                'calendar',
                'akce',
                'události',
                'event',
                'Kultura a volný čas',
              ];

              const monthNames = [
                'leden',
                'únor',
                'březen',
                'duben',
                'květen',
                'červen',
                'červenec',
                'srpen',
                'září',
                'říjen',
                'listopad',
                'prosinec',
                'january',
                'february',
                'march',
                'april',
                'may',
                'june',
                'july',
                'august',
                'september',
                'october',
                'november',
                'december',
              ];

              let found = false;

              function normalize(text) {
                return text
                  .normalize('NFD')
                  .replace(/[\u0300-\u036f]/g, '')
                  .toLowerCase();
              }

              function hasSequentialNumbers(text) {
                const numbers = text.match(/\d+/g);
                if (!numbers || numbers.length < 5) return false;

                let consecutiveCount = 1;

                for (let i = 1; i < numbers.length; i++) {
                  if (parseInt(numbers[i]) === parseInt(numbers[i - 1]) + 1) {
                    consecutiveCount++;

                    if (consecutiveCount >= 5) return true;
                  } else {
                    consecutiveCount = 1;
                  }
                }

                return false;
              }

              $body.find('*').each((i, el) => {
                const $el = Cypress.$(el);
                const attributes = [
                  $el.attr('class'),
                  $el.attr('id'),
                  $el.attr('aria-label'),
                ].map((attr) => normalize(attr || ''));
                const text = normalize($el.text().trim());
                const words = text.split(/\s+/);

                const matchesKeyword = calendarKeywords.some((keyword) => {
                  const normalizedKeyword = normalize(keyword);
                  const keywordWords = normalizedKeyword.split(/\s+/).length;

                  return (
                    attributes.some((attr) => attr === normalizedKeyword) ||
                    (text.includes(normalizedKeyword) &&
                      words.length <= keywordWords + 1)
                  );
                });

                if (matchesKeyword) {
                  const $parent = $el.parent();
                  const parentText = normalize($parent.text());
                  const containsMonth = monthNames.some((month) =>
                    parentText.split(/\s+/).includes(normalize(month)),
                  );
                  const containsSequence = hasSequentialNumbers(parentText);

                  if (containsMonth || containsSequence) {
                    cy.wrap($el);
                    found = true;
                    return false;
                  }
                }
              });
              return cy.wrap(found);
            });
          });
        });
      })
      .then((result) => {
        cy.task('updateCity', {
          id: city.id,
          city: { hasEventCalendar: result },
        });
        if (result) {
          cy.log('Test Passed: Calendar were found.');
        } else {
          cy.log('Test Failed: No Calendar were found.');
        }
      });
  });

  /**
   * Test to validate the presence and accessibility of forms on the city website.
   *
   * This test checks the main and contact pages for form elements. It performs accessibility
   * checks on each form, focusing on critical and serious violations while ignoring color contrast.
   * The number of forms and violations found are logged, and the city's database record is updated
   * to indicate whether the forms are valid based on the accessibility checks.
   *
   * Usage:
   * - Ensures the city website has accessible forms that meet certain WCAG standards.
   * - Updates the city's record with the results of the form validation.
   */
  it('should have a valid forms', { taskTimeout: 90000 }, () => {
    let formsCount = 0;
    let violationsCount = 0;

    ensureMinimumContactLinks(url, city.contactLinks)
      .then((contactLinks) => {
        validateContactLinks(contactLinks).then((validLinks) => {
          checkContentOnPages(validLinks, () => {
            // cy.get('body')
            //   .find('form')
            //   .then(($forms) => {
            cy.get('body').then(($body) => {
              if ($body.find('form').length > 0) {
                cy.get('body')
                  .find('form')
                  .then(($forms) => {
                    cy.log('Forms for testing: ', $forms.length);
                    if ($forms.length > 0) {
                      formsCount += $forms.length;
                      cy.injectAxe();
                      cy.checkA11y(
                        { include: 'form' },

                        {
                          includedImpacts: ['critical', 'serious'],
                          rules: {
                            'color-contrast': { enabled: false },
                          },
                          retries: 2,
                        },
                        (violations) => {
                          if (violations.length > 0) {
                            violationsCount += violations.length;
                          }
                          // cy.log(`Form violations: ${violations.length}`);
                          cy.task('writeReport', {
                            filename: `data/a11y-report-form.json`,
                            content: violations,
                          });
                        },
                        true,
                      );
                    }
                  });
              }
            });
            return cy.wrap(formsCount > 2 || violationsCount > 0);
          });
        });
      })
      .then(() => {
        if (formsCount > 2 || violationsCount > 0) {
          cy.task('updateCity', {
            id: city.id,
            city: { hasCorrectForms: violationsCount === 0 },
          });
        }
      });
  });

  /**
   * Test to ensure that the city website includes GDPR-related information or links.
   *
   * This test searches the website and linked pages for keywords commonly associated with GDPR
   * and data protection, such as "GDPR," "Privacy Policy," or "Data Protection Declaration."
   * If a matching link or text is found, the test passes, and the city's database record is updated
   * to reflect the presence of GDPR information. Otherwise, it logs a failure indicating no GDPR
   * page was found.
   *
   * Usage:
   * - Ensures the city website complies with GDPR requirements by providing relevant information.
   * - Updates the city's record with the result of the GDPR compliance check.
   */
  it('should find GDPR on the website', { retries: 2 }, () => {
    ensureMinimumContactLinks(url, city.contactLinks)
      .then((contactLinks) => {
        validateContactLinks(contactLinks).then((validLinks) => {
          checkContentOnPages(validLinks, () => {
            return cy.get('body').then(($body) => {
              let found = false;
              const gdprKeywords = [
                /gdpr/i,
                /zpracovani osobnich udaju/i,
                /ochrana osobnich udaju/i,
                /zasady ochrany osobnich udaju/i,
                /prohlaseni o ochrane soukromi/i,
                /povinne zverejnovane informace a gdpr/i,
                /prohlaseni o pristupnosti/i,
              ];

              function normalizeText(text) {
                return text
                  .normalize('NFD')
                  .replace(/[\u0300-\u036f]/g, '')
                  .toLowerCase();
              }

              $body.find('a').each((i, link) => {
                const $link = Cypress.$(link);
                const href = normalizeText($link.attr('href') || '');
                const linkText = normalizeText($link.text().trim());

                const hrefContainsGDPR = gdprKeywords.some((keyword) =>
                  keyword.test(href),
                );

                const textIsOnlyGDPRKeyword = gdprKeywords.some(
                  (keyword) =>
                    keyword.test(linkText) &&
                    linkText.match(keyword)[0] === linkText,
                );

                if (hrefContainsGDPR || textIsOnlyGDPRKeyword) {
                  found = true;
                  cy.wrap($link).scrollIntoView();
                  return false;
                }
              });

              return cy.wrap(found);
            });
          });
        });
      })
      .then((result) => {
        cy.task('updateCity', {
          id: city.id,
          city: { providesPrivacyPolicy: result },
        });
        if (result) {
          cy.log('Test Passed: GDPR page was found.');
        } else {
          cy.log('Test Failed: No GDPR page was found.');
        }
      });
  });

  /**
   * Skipped test that verifies if focusable elements display a visual change when focused - deprecated func from PoC.
   *
   * Checks if elements such as links, buttons, and inputs indicate focus by altering their style (e.g., outline).
   * Helps ensure that the page adheres to accessibility guidelines for focus indication.
   */
  it.skip('má zobrazovat ohraničení nebo změnu u klikatelných prvků při focusu', () => {
    cy.visit(url); // Nahraďte URL adresou testovaného webu
    cy.wait(1000);
    const selectors = ['a', 'button', 'input[type="submit"]'];
    selectors.forEach((selector) => {
      cy.get(selector).each(($el, index) => {
        // const styleBefore = $el.css('outline');
        // cy.wrap($el).focus();
        // // Ověření, že element je ve stavu focus
        // // Místo konkrétního stylu se zaměříme na aktivní (fokusovaný) stav
        // // cy.wrap($el).should("have.focus");
        // cy.wrap($el).then($elAfter => {
        //   // Získání stylů po fokusování
        //   const styleAfter = $elAfter.css('outline'); // Stejná vlastnost pro porovnání

        //   // Porovnání stylů před a po fokusování
        //   if (styleBefore !== styleAfter) {
        //     // Element ukazuje vizuální změnu, což indikuje, že je focusovatelný
        //     console.log('Element zobrazuje grafickou změnu při fokusování.');
        //   } else {
        //     // Žádná vizuální změna nebyla detekována
        //     console.log('Element nezobrazuje žádnou grafickou změnu při fokusování.');
        //   }
        // });

        // cy.wrap($el).then(($elBefore) => {
        //   // Získání stylů před fokusováním
        //   const styleBefore = $elBefore.css("outline"); // Můžete zkusit různé vlastnosti

        //   cy.wrap($el).focus();

        //   cy.wrap($el).then(($elAfter) => {
        //     // Získání stylů po fokusování
        //     const styleAfter = $elAfter.css("outline"); // Stejná vlastnost pro porovnání

        //     // Porovnání stylů před a po fokusování
        //     if (styleBefore !== styleAfter) {
        //       // Element ukazuje vizuální změnu, což indikuje, že je focusovatelný
        //       cy.log("Element zobrazuje grafickou změnu při fokusování.");
        //     } else {
        //       // Žádná vizuální změna nebyla detekována
        //       cy.log(
        //         "Element nezobrazuje žádnou grafickou změnu při fokusování."
        //       );
        //     }
        //   });
        // });

        // cy.wrap($el).then($elBefore => {
        //   // Emulace :focus-visible efektu
        //   $elBefore.addClass('focus-visible-emulation'); // Přidání třídy pro emulaci
        //   const styleBefore = $elBefore.css('outline');

        //   cy.wrap($el).focus();

        //   cy.wrap($el).then($elAfter => {
        //     // Emulace :focus-visible efektu
        //     $elAfter.addClass('focus-visible-emulation'); // Přidání třídy pro emulaci
        //     const styleAfter = $elAfter.css('outline');

        //     if (styleBefore !== styleAfter) {
        //       cy.log('Element zobrazuje grafickou změnu při fokusování s emulací :focus-visible.');
        //     } else {
        //       cy.log('Element nezobrazuje žádnou grafickou změnu při fokusování s emulací :focus-visible.');
        //     }
        //     $elAfter.removeClass('focus-visible-emulation'); // Odstranění třídy pro další testy
        //   });
        // });

        cy.get('button')
          .first()
          .then(($btn) => {
            // Získání stylů před fokusováním
            const styleBeforeFocus = $btn.css('outline');

            // Aplikace focus a porovnání stylů
            cy.wrap($btn)
              .focus()
              .then(($btnFocused) => {
                const styleAfterFocus = $btnFocused.css('outline');

                // Porovnání, zda došlo k nějaké změně
                // expect(styleBeforeFocus).not.to.eq(styleAfterFocus);
                if (styleBeforeFocus !== styleAfterFocus) {
                  cy.log('Zobrazuje');
                } else {
                  cy.log('Nezobrazuje');
                }
              });
          });
      });
    });
  });
});
