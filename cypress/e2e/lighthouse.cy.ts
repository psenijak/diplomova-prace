import { LightHouseEmulatorSize } from '../../src/types/lighthouse-emulator-size';
import { getLightHouseEmulatorSize } from '../support/lighthouse-device-settings';

/**
 * Cypress test suite for running a Lighthouse analysis on a city website.
 *
 * This test visits a specified city website and performs a Lighthouse audit to evaluate various quality metrics,
 * such as performance, accessibility, best practices, SEO, and PWA. The device type for emulation is determined
 * from the Cypress environment configuration and can be adjusted to test on different device types (e.g., PC, tablet, mobile).
 *
 * Configuration:
 * - The test suite has extended timeouts to accommodate potentially long Lighthouse audits.
 *
 * Usage:
 * - The base URL for the website and the device type are specified through Cypress environment variables.
 * - The test is suitable for automated quality assessments across various device emulations.
 */

describe(
  'Lighthouse',
  {
    execTimeout: 600000,
    requestTimeout: 600_000,
    defaultCommandTimeout: 600_000,
  },
  () => {
    const url = Cypress.env('baseUrl') || '';
    const device: LightHouseEmulatorSize = Cypress.env('device') || 'PC';
    it("should visit the city website and analyze it's quality", () => {
      cy.visit(url);
      cy.lighthouse(
        {
          performance: 0,
          accessibility: 0,
          'best-practices': 0,
          seo: 0,
          pwa: 0,
        },
        getLightHouseEmulatorSize(device),
      );
    });
  },
);
