import { City } from '@prisma/client';
import { getWikiDataForCity } from '../support/utils';

/**
 * Cypress test suite for fetching and storing website URLs and other details for cities.
 *
 * This test iteratively retrieves city data in batches and uses the `getWikiDataForCity` function
 * to visit each city's Wikipedia page, extracting and storing website URLs and other specifications.
 * It continues processing until no more cities without websites are found.
 *
 * Usage:
 * - The test uses a custom Cypress task, `findManyCitiesWithoutWebsite`, to fetch batches of cities without a website.
 * - It waits briefly between each batch to avoid overloading the server.
 *
 * Note:
 * - The test is configured to run in isolation with `.only` to focus on this specific functionality.
 * - Suitable for use in data-gathering or web-scraping tasks within a test automation context.
 */
describe('Get website url for each city', () => {
  const iterateOverStoredCityPages = (page: number): void => {
    cy.task('findManyCitiesWithoutWebsite', page).then((cities: City[]) => {
      if (cities.length === 0) {
        return;
      }

      cities.forEach((city: City) => {
        getWikiDataForCity(city);
      });

      cy.wait(1000).then(() => {
        iterateOverStoredCityPages(page + 1);
      });
    });
  };

  it.only('Visit each website and store city website and other specifications', () => {
    iterateOverStoredCityPages(1);
  });
});
