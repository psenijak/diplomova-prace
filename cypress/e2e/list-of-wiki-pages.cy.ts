/**
 * Cypress test suite for listing cities from Wikipedia.
 *
 * This test navigates to the Wikipedia page containing a list of cities in the Czech Republic.
 * It iterates through each table and row to extract city names and their corresponding Wikipedia URLs.
 * Each city's information is then stored using a custom Cypress task, `insertUniqueCity`.
 *
 * Usage:
 * - This test is intended for automated data scraping to collect city information from Wikipedia.
 * - The test checks each row of a '.wikitable' element and looks for links in the second column.
 */
describe('List of Cities', () => {
  it('Visit wikipedia and store all cities and their links', () => {
    cy.visit('https://cs.wikipedia.org/wiki/Seznam_obc%C3%AD_v_%C4%8Cesku');
    cy.get('.wikitable').each(($krajskaTabulka) => {
      cy.wrap($krajskaTabulka)
        .find('tr')
        .each(($radek) => {
          if ($radek.find('td:nth-child(2) a').length) {
            cy.wrap($radek)
              .find('td:nth-child(2) a')
              .each(($odkazNaMesto) => {
                const urlMesta = $odkazNaMesto.prop('href');

                cy.task('insertUniqueCity', {
                  name: $odkazNaMesto.text(),
                  wiki_url: urlMesta,
                });
              });
          }
        });
    });
  });
});
