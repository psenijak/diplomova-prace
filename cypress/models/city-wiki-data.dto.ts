export class CityWikiData {
  districtId?: number;
  citizens?: number;
  websiteUrl?: string;
}
