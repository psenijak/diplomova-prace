import { transformToSecuredUrl } from '../utils/url-transformer';

/**
 * Checks if a given URL is secure by ensuring it responds correctly to HTTPS requests and appropriately redirects from HTTP.
 *
 * This function performs two main checks:
 * 1. It attempts to access the URL using HTTPS, expecting a 200 status code. If the URL is redirected to an HTTP URL,
 *    it marks the page as insecure.
 * 2. It requests the original URL without following redirects and checks if it either stays on HTTPS or
 *    correctly redirects to HTTPS, indicating a secure setup. It further verifies that any redirection uses secure HTTP status codes.
 *
 * The result of the security check is wrapped in a Cypress Chainable object and stored as 'isPageSecure'.
 *
 * @param {string} url - The URL to check for security.
 * @returns {void} - Stores the result in a Cypress alias without returning.
 *
 * Usage:
 * - Use this function within Cypress tests to validate the security configuration of a web page,
 *   particularly to ensure that it enforces HTTPS and properly redirects from HTTP to HTTPS when necessary.
 */
export const checkSecurityOfPage = (url: string): void => {
  let isSecure = true;

  cy.request({
    url: transformToSecuredUrl(url),
    followRedirect: true,
  }).then((resp) => {
    if (resp.redirectedToUrl && resp.redirectedToUrl.startsWith('http://')) {
      isSecure = false;
    } else {
      expect(resp.status).to.eq(200);
      if (resp.status !== 200) {
        isSecure = false;
      }
    }
  });

  cy.request({
    url: url,
    followRedirect: false,
  }).then((resp) => {
    if (resp.redirectedToUrl) {
      if (!resp.redirectedToUrl.startsWith('https://')) {
        isSecure = false;
      }
    } else if (!(resp.status === 301 || resp.status === 302)) {
      isSecure = false;
    }
  });

  cy.wrap(isSecure).as('isPageSecure');
};
