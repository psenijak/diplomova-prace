/**
 * Gathers contact-related hyperlinks from the current webpage that match specific keywords and are from the same domain.
 *
 * This function searches for links that contain contact-related keywords in their href attributes or text content,
 * excluding those that start with 'mailto:' or 'tel:'. It validates the links to ensure they belong to the same domain
 * as the base URL or the current page URL. The collected links are then wrapped in a Cypress Chainable object.
 *
 * @param {string} baseUrl - The base URL of the site, used to validate the domain of found links.
 * @returns {Cypress.Chainable<string[]>} - A Cypress Chainable object containing an array of validated URLs.
 *
 * Usage:
 * - This function can be used in Cypress tests to collect URLs for pages like contact forms, about us, etc.,
 *   that are pertinent to customer support or other direct contacts.
 */

export const gatherContactLinksOnPage = (
  baseUrl: string,
): Cypress.Chainable<string[]> => {
  const keywords: RegExp[] = [
    /kontakty?/i,
    /contacts?/i,
    /napište nám/i,
    /úřední hodiny/i,
  ];
  const suitableLinks: string[] = [];
  const baseUrlHostname = new URL(baseUrl).hostname;

  return cy.window().then((win) => {
    const currentPageHostname = win.location.hostname;

    return cy
      .get('a')
      .each(($a) => {
        const hrefProp = $a.prop('href');
        const href = typeof hrefProp === 'string' ? hrefProp.toLowerCase() : '';

        const text = $a.text().toLowerCase();
        const matchesHref = keywords.some((keyword) => keyword.test(href));
        const matchesText = keywords.some((keyword) => keyword.test(text));

        if (
          !href.startsWith('mailto:') &&
          !href.startsWith('tel:') &&
          (matchesHref || matchesText)
        ) {
          try {
            const url = new URL(href, baseUrl);
            if (
              url.hostname === baseUrlHostname ||
              url.hostname === currentPageHostname
            ) {
              suitableLinks.push(url.href);
            }
          } catch (error) {
            cy.log(
              `Failed to construct URL with href: ${href} - Error: ${
                error instanceof Error ? error.message : String(error)
              }`,
            );
          }
        }
      })
      .then(() => {
        return cy.wrap(suitableLinks);
      });
  });
};
