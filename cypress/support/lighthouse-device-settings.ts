import { LightHouseEmulatorSize } from '../../src/types/lighthouse-emulator-size';

/**
 * Retrieves device-specific emulation settings for Lighthouse testing.
 *
 * @param {LightHouseEmulatorSize} deviceType - Enum value representing the device ('PC', 'TABLET', 'MOBILE').
 * @returns {object | null} - Configuration for the specified device or null if the type is unrecognized.
 *
 * This function configures emulation settings such as form factor, screen size, and scaling
 * based on the specified device type, aiding in responsive testing.
 */
export const getLightHouseEmulatorSize = (
  deviceType: LightHouseEmulatorSize,
): object | null => {
  switch (deviceType) {
    case 'PC':
      return {
        formFactor: 'desktop',
        screenEmulation: {
          mobile: false,
          disable: false,
          width: 1920,
          height: 1080,
          deviceScaleRatio: 1,
        },
      };
    case 'TABLET':
      return {
        deviceName: 'Nexus 10',
        screenEmulation: {
          disabled: false,
          deviceScaleFactor: 2,
          mobile: true,
          width: 800,
          height: 1280,
        },
      };
    case 'MOBILE':
      return {
        formFactor: 'mobile',
        screenEmulation: {
          mobile: true,
          width: 390,
          height: 844,
          deviceScaleFactor: 3,
          disabled: false,
        },
      };
    default:
      return {
        formFactor: 'desktop',
        screenEmulation: {
          mobile: false,
          disable: false,
          width: 1920,
          height: 1080,
          deviceScaleRatio: 1,
        },
      };
  }
};
