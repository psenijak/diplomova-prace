/**
 * Retrieves HTML elements containing phone numbers from the current webpage.
 *
 * This function uses a predefined set of CSS selectors to find elements that likely contain phone numbers,
 * then filters these elements by checking for patterns typical of phone numbers. The matched elements are
 * stored in a Cypress alias for later use in tests.
 *
 * @returns {void} - Elements are wrapped and stored as a Cypress alias if found.
 *
 * Usage:
 * This function should be used within a Cypress test to capture elements for assertions or further interactions.
 * The elements found are accessible via the 'mobileNumberElementList' alias in subsequent test steps.
 */
export const getElementsWithPhoneNumbers = (): any => {
  const selectors = [
    'a[href^="tel:"], address, .copy-kontakt, p:contains("Tel"), p:contains("telefon"), p:contains("Telefon")',
    'tr:has(i[class*="fa-phone"]), tr:has(i[class*="fa-mobile"])',
    'span:contains("ústředna tel."), span:contains("tel.:"), span:contains("tel")',
    'td:contains("tel.:")',
  ];

  const fullSelector = selectors.join(', ');
  const matchedElements: HTMLElement[] = [];

  cy.get(fullSelector, { timeout: 1000 }).then(($elements) => {
    if ($elements.length) {
      const phoneNumberPattern =
        /(\+420)?[ ]?\d{3}[\s\u00A0]\d{3}[\s\u00A0]\d{3}|(\+420)?[ ]?\d{3}[\s\u00A0]\d{2}[\s\u00A0]\d{3}[\s\u00A0]\d{2}/g;

      $elements.each((index, element) => {
        const text = Cypress.$(element).text();
        if (text.match(phoneNumberPattern)) {
          matchedElements.push(element);
        }
      });
    }
    if (matchedElements.length > 0) {
      cy.wrap(matchedElements).as('mobileNumberElementList');
    } else {
      cy.wrap([]).as('mobileNumberElementList');
    }
  });
};
