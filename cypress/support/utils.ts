import { City, District, Region } from '@prisma/client';
import { CityWikiData } from '../models/city-wiki-data.dto';

/**
 * Iterates over a specific wikitable to extract city data and store it using a Cypress task.
 *
 * This function uses Cypress to select a wikitable by index, then navigates through its rows to extract URLs and names of cities.
 * Each city's information is stored via a Cypress task. Designed for use within a Cypress test that interacts with webpages containing structured data.
 *
 * @param {number} index - The index of the wikitable to interact with, based on zero-based positioning.
 *
 * Note:
 * - Assumes the presence of '.wikitable' in the DOM.
 * - Relies on city links being located in the second column of the table.
 * - Incorporates brief delays to manage request timing.
 */
export const iterateOverWikitable = (index: number) => {
  cy.get('.wikitable')
    .eq(index)
    .then(($krajskaTabulka) => {
      cy.wrap($krajskaTabulka)
        .find('tr')
        .each(($radek) => {
          if ($radek.find('td:nth-child(2) a').length) {
            cy.wrap($radek)
              .find('td:nth-child(2) a')
              .each(($odkazNaMesto) => {
                const urlMesta = $odkazNaMesto.prop('href');

                cy.task('insertCity', {
                  name: $odkazNaMesto.text(),
                  wiki_url: urlMesta,
                });
                cy.wait(20);
              });
          }
        });
    });
};

/**
 * Handles the storage or retrieval of a district by its URL. If existing, it updates the city data with the district ID.
 * If not, it creates a new district entry.
 *
 * @param {Region} region - Region object containing the region's ID.
 * @param {string} districtName - Name of the district.
 * @param {string} districtUrl - URL of the district wiki page.
 * @param {CityWikiData} cityData - Object containing city data to be updated.
 */
const handleDistrictStore = (
  region: Region,
  districtName: string,
  districtUrl: string,
  cityData: CityWikiData,
) => {
  cy.task('findDistrictByUrl', districtUrl).then((district: District) => {
    if (district) {
      cityData.districtId = district.id;
    } else {
      cy.task('createDistrict', {
        regionId: region.id,
        name: districtName,
        wiki_url: districtUrl,
      }).then((district: District) => {
        cityData.districtId = district.id;
      });
    }
  });
};

/**
 * Handles the storage or retrieval of a region by its URL. Upon successful retrieval or creation,
 * it proceeds to handle district storage.
 *
 * @param {CityWikiData} cityData - Object containing city data.
 * @param {string} regionUrl - URL of the region wiki page.
 * @param {string} regionName - Name of the region.
 * @param {string} districtName - Name of the district.
 * @param {string} districtUrl - URL of the district wiki page.
 */
const handleRegionStore = (
  cityData: CityWikiData,
  regionUrl: string,
  regionName: string,
  districtName: string,
  districtUrl: string,
) => {
  cy.task('findRegionByUrl', regionUrl).then((region: Region | null) => {
    if (region) {
      handleDistrictStore(region, districtName, districtUrl, cityData);
    } else {
      cy.task('createRegion', {
        wiki_url: regionUrl,
        name: regionName,
      }).then((region: Region) => {
        handleDistrictStore(region, districtName, districtUrl, cityData);
      });
    }
  });
};

/**
 * Initiates the data retrieval process for city-related data from its Wikipedia page.
 * It fetches and processes data related to the city's region, district, population, and official website.
 *
 * @param {City} city - City object containing the Wikipedia URL and other relevant info.
 */
export const getWikiDataForCity = (city: City) => {
  cy.visit(city.wiki_url);
  const cityData = new CityWikiData();

  cy.get('th:contains("Kraj")')
    .closest('tr')
    .then(($row) => {
      const $links = $row.find('td a');

      if ($links.length > 0) {
        const regionName = $links.first().text();
        const regionUrl = $links.first().prop('href');

        cy.get('th:contains("Okres")')
          .closest('tr')
          .then(($rowDistrict) => {
            const $linksDistrict = $rowDistrict.find('td a');

            if ($linksDistrict.length > 0 && regionUrl) {
              const districtName = $linksDistrict.first().text();
              const districtUrl = $linksDistrict.first().prop('href');
              handleRegionStore(
                cityData,
                regionUrl,
                regionName,
                districtName,
                districtUrl,
              );
            }
          });
      } else {
        cy.log('Město: ', city.name, ' nemá okres a kraj!');
      }
    })
    .then(() => {
      return cy
        .get('th:contains("Počet obyvatel")')
        .closest('tr')
        .find('td')
        .invoke('text')
        .then((text) => {
          const parts = text.split(' (');
          const populationText = parts[0].trim();

          const cleanText = populationText.replace(/[^0-9]/g, '');
          const population = parseInt(cleanText, 10);
          cityData.citizens = population;
        });
    })
    .then(() => {
      cy.get('body').then(($body) => {
        const $links = $body.find("td:contains('Oficiální web:') a");

        if ($links.length) {
          const websiteUrl = $links.first().attr('href');

          if (websiteUrl) {
            cityData.websiteUrl = websiteUrl;
            cy.log('Nalezený oficiální web: ', websiteUrl);
          }
        }
      });
    })
    .then(() => {
      if (cityData.citizens) {
        cy.task('updateCity', {
          id: city.id,
          citizens: cityData.citizens,
          website_url: cityData.websiteUrl,
          districtId: cityData.districtId,
        });
      }
    });
};
