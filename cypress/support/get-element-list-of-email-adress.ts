/**
 * Scans all elements within the body of the current webpage and identifies those containing valid email addresses.
 *
 * This function searches through all page elements for text that matches a standard email pattern.
 * If multiple instances of the same email are found, it prioritizes `<a>` tags over others. Each unique
 * email address is stored once along with its corresponding HTML element in a Map, which is then converted to an array.
 * The elements are wrapped in a Cypress alias for subsequent use.
 *
 * @returns {void} - The found elements are wrapped and stored as a Cypress alias 'emailsElementList'.
 *
 * Usage:
 * Utilize this function within a Cypress test to gather elements that contain email addresses for validations or interactions.
 * Access the stored elements via the 'emailsElementList' alias in your test steps.
 */
export const getElementsWithEmails = (): any => {
  const emailPattern = /^[\w.-]+@[\w-]+\.[a-zA-Z]{2,3}$/g;

  cy.get('body').then(($body) => {
    const allElements = $body.find('*');
    const emailMap = new Map();

    allElements.each(function (index, element) {
      const text = Cypress.$(element).text();
      if (emailPattern.test(text)) {
        emailPattern.lastIndex = 0;
        const email = text.match(emailPattern)[0];

        if (emailMap.has(email)) {
          const existingElement = emailMap.get(email);

          if (element.tagName === 'A' && existingElement.tagName !== 'A') {
            emailMap.set(email, element);
          }
        } else {
          emailMap.set(email, element);
        }
      }
    });
    cy.log(Array.from(emailMap).toString());

    const matchedElements = Array.from(emailMap.values());

    cy.wrap(matchedElements).as('emailsElementList');
  });
};
