type ContentCheckFunction = () => Cypress.Chainable<boolean>;

/**
 * Recursively checks a list of URLs for specific content using a callback function.
 *
 * This function attempts to find specified content on a series of web pages. It uses a callback
 * to determine the presence of the content on the current page. If the content is not found,
 * it moves to the next URL in the list. The process continues until the content is found or all
 * URLs are checked. Requests are made to ensure the URLs are reachable before navigating to them.
 *
 * @param {string[]} urls - An array of URLs to check.
 * @param {ContentCheckFunction} contentCheckCallback - A callback function that returns a Cypress.Chainable<boolean>
 *                                                      indicating whether the content is found on the current page.
 * @returns {Cypress.Chainable<boolean>} - A Cypress Chainable object that resolves to true if the content is found,
 *                                         otherwise false.
 *
 * Usage:
 * - This function is ideal for verifying the presence of required content across multiple pages within a test suite,
 *   especially when the content might be dynamically located across several pages.
 */
export const checkContentOnPages = (
  urls: string[],
  contentCheckCallback: ContentCheckFunction,
): Cypress.Chainable<boolean> => {
  return contentCheckCallback().then((contentFound) => {
    if (contentFound) {
      cy.log('Required content found on the current page.');
      return cy.wrap(true);
    } else if (urls.length > 0) {
      const nextURL = urls.shift();
      cy.log(`Content not found, checking next URL: ${nextURL}`);

      return cy
        .request({
          url: nextURL,
          failOnStatusCode: false,
        })
        .then((response) => {
          if (response.status >= 200 && response.status < 400) {
            return cy.visit(nextURL).then(() => {
              return checkContentOnPages(urls, contentCheckCallback);
            });
          } else {
            cy.log(
              `Page request failed with status: ${response.status}. Trying next URL.`,
            );

            if (urls.length > 0) {
              return checkContentOnPages(urls, contentCheckCallback);
            } else {
              cy.log('Content not found on any of the checked pages.');
              return cy.wrap(false);
            }
          }
        });
    } else {
      cy.log('Content not found on any of the checked pages.');
      return cy.wrap(false);
    }
  });
};
