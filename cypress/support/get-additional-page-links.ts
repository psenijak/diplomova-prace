/// <reference types="cypress" />

/**
 * Checks if city.contactLinks has at least 5 elements. If not, gathers up to 30 links
 * from the current page that point to the same hostname.
 * @param {string} baseUrl - The base URL of the site.
 * @param {string[]} contactLinks - An array of existing contact links.
 * @returns {Cypress.Chainable<string[]>} - Returns an updated list of contact links.
 */

export const ensureMinimumContactLinks = (
  baseUrl: string,
  contactLinks: string[],
): Cypress.Chainable<string[]> => {
  const hostname = new URL(baseUrl).hostname;
  const suitableLinks: Set<string> = new Set(contactLinks);

  return cy.window().then((win) => {
    const currentPageHostname = win.location.hostname;

    return cy
      .get('a')
      .each(($a) => {
        const hrefProp = $a.prop('href');
        const href = typeof hrefProp === 'string' ? hrefProp.toLowerCase() : '';

        try {
          const url = new URL(href, baseUrl);
          if (
            url.hostname === hostname ||
            url.hostname === currentPageHostname
          ) {
            suitableLinks.add(url.href);
          }
        } catch (error) {
          cy.log(
            `Failed to construct URL with href: ${href} - Error: ${
              error instanceof Error ? error.message : String(error)
            }`,
          );
        }

        if (suitableLinks.size >= 30) {
          return false;
        }
      })
      .then(() => {
        if (suitableLinks.size < 30) {
          cy.log(
            `Found only ${suitableLinks.size} links, trying to gather more...`,
          );
        }
        const finalLinks = Array.from(suitableLinks).slice(0, 30);
        return cy.wrap(finalLinks);
      });
  });
};
