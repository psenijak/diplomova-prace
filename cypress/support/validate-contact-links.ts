/**
 * Validates a list of URLs to ensure they are reachable (e.g., do not return 404 status).
 * @param {string[]} links - An array of URLs to validate.
 * @returns {Cypress.Chainable<string[]>} - A promise resolving to a list of valid URLs.
 */
export const validateContactLinks = (
  links: string[],
): Cypress.Chainable<string[]> => {
  const validLinks: string[] = [];

  return cy.wrap(null).then(() => {
    const promises = links.map((link) => {
      return cy.task('checkUrlStatus', link).then((result: any) => {
        if (result.status === 'valid') {
          validLinks.push(link);
        } else {
          cy.log(
            `Invalid link: ${link} with status ${result.statusCode || result.error}`,
          );
        }
      });
    });

    return Cypress.Promise.all(promises).then(() => validLinks);
  });
};
