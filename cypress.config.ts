/* eslint-disable @typescript-eslint/no-unused-expressions */
/* eslint-disable @typescript-eslint/no-floating-promises */
import fs from 'fs';
import https from 'https';
import { lighthouse, prepareAudit } from '@cypress-audit/lighthouse';
// import { lighthouse, prepareAudit }from "@cypress-audit/lighthouse";
import { City, District, Prisma, PrismaClient, Region } from '@prisma/client';
import axios from 'axios';
import { defineConfig } from 'cypress';
import { LightHouseEmulatorSize } from './src/types/lighthouse-emulator-size';

export const PAGE_SIZE = 100;

let prisma: PrismaClient;

if (prisma === undefined) {
  prisma = new PrismaClient();
}

export default defineConfig({
  chromeWebSecurity: false,
  experimentalModifyObstructiveThirdPartyCode: true,
  e2e: {
    setupNodeEvents(on, config) {
      on('before:browser:launch', (browser?: any, launchOptions?) => {
        prepareAudit(launchOptions);
      }),
        // on('before:browser:launch', (browser, launchOptions) => {

        //   prepareAudit(launchOptions);
        //   if (browser.name === 'chrome' && browser.isHeadless) {
        //   launchOptions.args.push('--disable-gpu');
        //   return launchOptions;
        //   }
        // });
        on('task', {
          // lighthouse: lighthouse(),
          // @ts-ignore
          lighthouse: lighthouse((lighthouseReport) => {
            const device: LightHouseEmulatorSize = config.env.device;

            fs.writeFileSync(
              `data/lighthouse-report${device && `-${device.toLocaleLowerCase()}`}.json`,
              JSON.stringify(lighthouseReport, null, 2),
              'utf8',
            );
            return lighthouseReport;
          }),
          writeReport({ filename, content }) {
            fs.writeFileSync(
              filename,
              JSON.stringify(content, null, 2),
              'utf8',
            );
            return null;
          },
          async checkUrlStatus(url) {
            try {
              const response = await axios.get(url, {
                httpsAgent: new https.Agent({
                  family: 4, // Forces IPv4
                }),
              });
              const contentType = response.headers['content-type'];
              if (
                response.status >= 200 &&
                response.status < 400 &&
                contentType.includes('text/html')
              ) {
                return { url, status: 'valid' };
              } else {
                return { url, status: 'invalid', statusCode: response.status };
              }
            } catch (error) {
              return { url, status: 'invalid', error: error.message };
            }
          },
          async insertUniqueCity(city: City): Promise<City> {
            try {
              const createdCity = await prisma.city.create({
                data: {
                  ...city,
                },
              });
              return createdCity;
            } catch (error) {
              if (error instanceof Prisma.PrismaClientKnownRequestError) {
                if (error.code !== 'P2002') {
                  throw error;
                }
              }
            }
            prisma.$disconnect();
            return city;
          },
          // async findCityById(id: number): Promise<City | null> {
          //   return await prisma.city.findUnique({
          //     where: { id },
          //   });
          // },

          async getCityPagesCount(): Promise<number> {
            const count = await prisma.city.count();
            return Math.ceil(count / PAGE_SIZE);
          },
          async findManyCitiesWithoutClassification(
            page: number,
          ): Promise<City[]> {
            return await prisma.city.findMany({
              skip: PAGE_SIZE * (page - 1),
              take: PAGE_SIZE,
            });
          },
          async findManyCitiesWithoutWebsite(page: number): Promise<City[]> {
            return await prisma.city.findMany({
              skip: PAGE_SIZE * (page - 1),
              take: PAGE_SIZE,
              where: {
                website_url: null,
              },
            });
          },
          async updateCity(data: {
            id: number;
            city: Partial<City>;
          }): Promise<null> {
            if (data.city) {
              await prisma.city.update({
                where: { id: data.id },
                data: data.city,
              });
            }
            return null;
          },
          async findRegionByUrl(url: string): Promise<Region | null> {
            return await prisma.region.findFirst({ where: { wiki_url: url } });
          },
          async createRegion(region: Region): Promise<Region> {
            return await prisma.region.create({ data: { ...region } });
          },
          async findDistrictByUrl(url: string): Promise<District | null> {
            return await prisma.district.findFirst({
              where: { wiki_url: url },
            });
          },
          async createDistrict(district: District): Promise<District> {
            return await prisma.district.create({ data: { ...district } });
          },
          async findCityByUrl(url: string): Promise<City | null> {
            return await prisma.city.findFirst({ where: { website_url: url } });
          },
        });
    },
  },
});
