# Analýza přístupnosti webových stránek obcí a měst v ČR

## Úvod

Tento projekt byl vytvořen jako součást diplomové práce na Vysoké škole ekonomické v Praze, Fakultě informatiky a statistiky. Cílem projektu je automatizovaná analýza webových stránek obcí a měst v České republice pomocí skriptů v Cypress, aby bylo možné hodnotit jejich přístupnost podle mezinárodních standardů a předpisů.

## Autor

- **Jakub Pšenička**
- Vedoucí práce: Ing. et Ing. Stanislav Vojíř, Ph.D.
- Kontakt na autora: [jakub.psenicka@email.com](mailto:jakub.psenicka@email.com)

## Licence

Tento projekt je dostupný pod MIT licencí, která umožňuje volné použití, distribuci a modifikaci software za předpokladu, že bude zachována stejná licence i pro odvozené díly.

## Popis aplikace

Tato aplikace slouží k automatizované analýze webových stránek obcí a měst v České republice, s cílem zjistit, do jaké míry tyto weby splňují základní principy přístupnosti a další specifická kritéria důležitá pro obecní weby. Program funguje tak, že uživatel zadá URL adresu obecního webu, na základě které aplikace spustí sérii testů zaměřených na různé aspekty webové přístupnosti a funkcionality.

### Funkcionalita

Aplikace začíná extrakcí obsahu zadaného webu pomocí URL. Následně analyzuje web podle předem stanovených kritérií, které zahrnují obecná pravidla přístupnosti, jak jsou definována mezinárodními standardy (např. WCAG – Web Content Accessibility Guidelines). Tato pravidla zahrnují aspekty jako je čitelnost textu, správné používání HTML značek pro strukturu obsahu, dostatečný kontrast mezi textem a pozadím, navigační prvky přístupné přes klávesnici, a mnoho dalších.

Kromě těchto obecných kritérií aplikace zahrnuje i specifika, která jsou příznačná pro obecní weby. Tyto zahrnují například přítomnost kontaktní stránky, úředních hodin, informací o veřejných službách a další obsah, který by měl být na těchto webech snadno dostupný.

### Omezení

Ačkoliv aplikace poskytuje užitečný nástroj pro rychlou analýzu kvality a přístupnosti obecních webů, jedná se o první verzi, která může obsahovat určité chyby a nepřesnosti. Některé aspekty webu, jako jsou dynamické obsahy nebo specifické interaktivní prvky, mohou být hodnoceny méně přesně, což vyžaduje další iterace vývoje aplikace pro zlepšení její přesnosti a spolehlivosti.

Tato aplikace je tedy významným prvním krokem k poskytnutí nástrojů pro obce a města, aby mohly lépe hodnotit a zlepšovat přístupnost svých webových stránek, což je klíčové pro zajištění rovného přístupu všem uživatelům, včetně osob s omezeními.

## Konfigurace a spuštění

Aplikace je napsaná v Node.js, a proto je pro spuštění nutné nainstalovat lokální instanci tohoto nástroje. Node.js je dostupný na [nodejs.org](https://nodejs.org/en/download/package-manager). Aplikace využívá pro správu balíčků Npm, který je součástí standardní distribuce Node.js, takže není potřeba řešit další nástroje, jako je Yarn.

Aplikace má kromě prostředí jedinou další závislost, kterou je databáze PostgreSQL.

Pro spuštění aplikace je nutné připojit ji k databázovému serveru. Toto se provádí pomocí souboru `.env`, který obsahuje konfiguraci pro připojení k databázi. Tento soubor je nutné vytvořit v kořenovém adresáři aplikace a vložit do něj proměnnou `DATABASE_URL`, která obsahuje standardní URL pro připojení k databázi (podrobněji na [Prisma docs](https://www.prisma.io/docs/orm/overview/databases/postgresql)), ve tvaru:

```bash
DATABASE_URL="postgresql://USER:PASSWORD@HOST:PORT/DATABASE"
```

Dále je potřeba provést instalaci externích závislostí. K tomu nám poslouží nástroj pro správu závislostí, který se automaticky instaluje s Node.js - Npm. Pro instalaci stačí v kořenovém adresáři spustit příkaz:

```bash
npm install
```

V aplikaci jsou definovány skripty, které slouží k formátování kódu a lintování. Skripty `format` a `lint` jsou zodpovědné za udržování kvality kódu, nicméně pro uživatele aplikace nejsou potřebné. Hlavním skriptem je `start`, který slouží ke spuštění aplikace:

```bash
npm run format
npm run lint
npm run start
```

Při spuštění hrají také roli argumenty, které je možné aplikaci předat. Tyto argumenty jsou jak ve formě `key=value`, tak i formě přepínačů, které jsou zpracovány pomocí knihovny yargs. Argumenty jsou popsány v tabulce níže:

| Název             | Proměnná      | Alias | Typ         | Poznámka                                                                       |
| ----------------- | ------------- | ----- | ----------- | ------------------------------------------------------------------------------ |
| URL               | url           | u     | string      | URL města, které má být zpracováno.                                            |
| Výpis             | print         | p     | flag        | Přepínač, který definuje, zda se mají vypsat výsledky z db bez nové analýzy.   |
| Množina           | set           | s     | [tr, ts, i] | Spuštění na celém setu - trénovací(tr), testovací(ts), ruční(i).               |
| Načtení           | load          | l     | flag        | Přepínač, který spustí iniciální načtení měst i Wikipedie do db.               |
| Rozdělení sady    | setSize       | z     | number      | Nastavená trénovací a testovací sady. Udává počet setů dohromady (default 30). |
| Počet trénovacích | trainingCount | r     | number      | Počet trénovacích prvků (default 21). Musí být menší než `setSize`.            |
| Validace          | checkSets     | c     | boolean     | Kontrola existence (default true). `false` vyplne kontrolu a přepíše data.     |
| Nápověda          | help          | h     | flag        | Vypíše nápovědu.                                                               |

Pro spuštění s argumenty musí být vždy vložen `--` před argumenty, což ukončuje argumenty pro npm skripty a začíná argumenty pro aplikaci. Příklad spuštění s argumenty:

```bash
npm run start -- --url=http://www.mukolin.cz/ -p
```

Je možné využít aliasy, které jsou popsány v tabulce výše. V aplikaci musí být vždy přítomen alespoň jeden z hlavních přepínačů, jinak se aplikace ukončí s chybou.

## Výpis výsledků

Jelikož nemá aplikace žádné grafické rozhraní, ve kterém by se mohl uživatel pohodlně pohybovat, bylo nutné vytvořit přehledný výstup, který bude v terminálu snadno čitelný, jelikož prezentace výsledků z analýzy je ten nejdůležitější výstup celé aplikace. Výstupních dat je poměrně mnoho a proto byl důležitým faktorem i formát výstupu, který musí být přehledný. Proto byly využity knihovny `cli-table3` a `cli-color`, které umožňují vytvářet tabulky a formátovat text v terminálu, což je pro uživatele přehlednější než pouhý text.

Výstup analýzy je v příkazové řádce prezentován pomocí dvou tabulek. První poskytuje data o analyzovaném webu. Tento výstup, který je pro ukázku proveden na webu města Kutná hora, je vidět níže na obrázku (náhled obrázku není ve formátu markdown k dispozici).

Pod tabulkou s informacemi o webu je druhá tabulka, která obsahuje výsledky jednotlivých kritérií.

Jednotlivá kritéria mají vždy naměřenou hodnotu a následně poznámku - vysvětlivku, která popisuje, co daná hodnota znamená. Zjištěné hodnoty jsou vždy barevně zvýrazněny. U boolean hodnot je to jednoduché, kde zelená značí správný výsledek a červená chybu. Dále jsou zde hodnoty, jak je kritérium SEO nebo Rychlost načítání, které jsou na procentuální škále od 0 až 100. Barevné zvýraznění je pak zelená, žlutá a červená, kde jednotlivé hranice jsou nastavené po vzoru nástroje Lighthouse.

Kritérium WCAG má jako výstup 4 hodnoty, kdy každá z nich udává celočíselnou hodnotu počtu zjištěných chyb s danou severitou - drobné, mírné, závažné a kritické. Barevné zvýraznění je zde nastaveno tak, že zelená značí 0 chyb na této úrovni, žlutá 1 - 10 chyb a červená více než 10 chyb.

Poslední hodnotou může být ve výstupu text "Nezjištěno", který je zobrazen na žlutém pozadí. Tento text značí situaci, kdy kritérium nebylo zjištěno buďto kvůli nějaké chybě, nebo také nemuselo být zjištěno z důvodu, že není relevantní. Například, pokud by web neobsahoval žádný formulář, není možné zjistit, zda je validní.

Script je také opatřen informací o časové náročnosti analýzy, která je vypsána na konci výstupu. Například je tedy uživateli zobrazeno `Doba trvání: 3m 46s 817ms`. Pro implementaci této utility byla vytvořena jednoduchá funkce, která ve formě hooku dokáže obalit libovolnou funkci, pro kterou časovou náročnost změří. Níže je vidět tato chytrá, ale jednoduchá funkce včetně následného použití:

```typescript
import { performance } from 'perf_hooks';

export const withTiming = <T extends (...args: any[]) => Promise<void>>(
  fn: T,
): T => {
  return async function (...args: any[]) {
    const startTime = performance.now();
    try {
      await fn(...args);
    } finally {
      const endTime = performance.now();
      const duration = endTime - startTime;
      const minutes = Math.floor(duration / 60000);
      const seconds = Math.floor((duration % 60000) / 1000);
      const milliseconds = Math.floor(duration % 1000);
      console.log(`\nDoba trvání: ${minutes}m ${seconds}s ${milliseconds}ms\n`);
    }
  } as T;
};

const timedIterateOverOneCity = withTiming(iterateOverOneCity);
await timedIterateOverOneCity(city.website_url, args.print);
```
