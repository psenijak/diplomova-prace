import { City, District, Region } from '@prisma/client';

export type CityWithRelations = City & {
  district?: District & {
    region?: Region;
  };
};
