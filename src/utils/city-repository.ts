import { City, PrismaClient, UsageType } from '@prisma/client';

let prisma: PrismaClient;

if (prisma === undefined) {
  prisma = new PrismaClient();
}

/**
 * Updates specific metrics for a city identified by its ID.
 * This function allows partial update of city properties.
 *
 * @param {number} cityId - The unique identifier for the city.
 * @param {Partial<City>} data - Partial city data to update.
 * @returns {Promise<void>} - Promise that resolves when the update is complete.
 */
export const storeMetricsForCity = async (
  cityId: number,
  data: Partial<City>,
): Promise<void> => {
  await prisma.city.update({
    where: { id: cityId },
    data,
  });
};

/**
 * Resets all measurable metrics for a city to their initial state (null or empty).
 *
 * @param {number} cityId - The ID of the city whose metrics are to be reset.
 * @returns {Promise<void>} - Promise that resolves when the reset is complete.
 */
export const resetCityMetrics = async (cityId: number): Promise<void> => {
  await prisma.city.update({
    where: { id: cityId },
    data: {
      hasSocialNetworks: null,
      hasCorrectSemanticLinks: null,
      hasContactForm: null,
      respectsCookiesConsent: null,
      providesPrivacyPolicy: null,
      hasEventCalendar: null,
      contactLinks: [],
      hasPhoneNumbersOnMainPage: null,
      hasEmailsOnMainPage: null,
      hasWorkingHours: null,
      hasOfficialBoard: null,
      hasCorrectHierarchyOfHeadings: null,
      hasBreadcrumbs: null,
      hasCorrectForms: null,
      hasResponsiveDesign: null,
      loadingPerformance: null,
      hasSiteMap: null,
      hasRobots: null,
      seoScore: null,
      hasSecuredCookies: null,
      hasHTTPS: null,
      hasALTAttributes: null,
      contrastProblems: null,
      hasARIAAttributes: null,
      WCAGMinorProblemCount: null,
      WCAGModerateProblemCount: null,
      WCAGSeriousProblemCount: null,
      WCAGCriticalProblemCount: null,
    },
  });
};

/**
 * Retrieves a city by its unique ID.
 *
 * @param {number} id - The ID of the city to retrieve.
 * @returns {Promise<City | null>} - A promise that resolves to the city object or null if not found.
 */
export const getCityById = async (id: number): Promise<City | null> => {
  return await prisma.city.findUnique({
    where: { id },
  });
};

/**
 * Verifies the existence of a city by its website URL.
 *
 * @param {string} url - The URL of the city to verify.
 * @returns {Promise<City | null>} - A promise that resolves to the city if found, otherwise null.
 */
export const verifyCityExistenceByUrl = async (
  url: string,
): Promise<City | null> => {
  const city = await prisma.city.findFirst({
    where: { website_url: url },
  });

  return city;
};

/**
 * Retrieves a city and its related district and region data by its website URL.
 *
 * @param {string} url - The URL of the city to find.
 * @returns {Promise<City | null>} - A city with its nested relations if found, otherwise null.
 */
export const getCityByUrlWithRelations = async (
  url: string,
): Promise<City | null> => {
  const city = await prisma.city.findFirst({
    where: { website_url: url },
    include: {
      district: {
        include: {
          region: true,
        },
      },
    },
  });

  return city;
};

/**
 * Retrieves all cities that match a specific usage type.
 *
 * @param {UsageType} usageType - The usage type to filter cities by.
 * @returns {Promise<City[]>} - A promise that resolves to an array of cities with the specified usage type.
 */
export const getCitiesSetByUsageType = async (
  usageType: UsageType,
): Promise<City[]> => {
  return await prisma.city.findMany({
    where: { usageType: { has: usageType } },
  });
};
