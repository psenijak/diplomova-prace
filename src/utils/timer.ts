import { performance } from 'perf_hooks';

export const withTiming = <T extends (...args: any[]) => Promise<void>>(
  fn: T,
): T => {
  return async function (...args: any[]) {
    const startTime = performance.now();
    try {
      await fn(...args);
    } finally {
      const endTime = performance.now();
      const duration = endTime - startTime;
      const minutes = Math.floor(duration / 60000);
      const seconds = Math.floor((duration % 60000) / 1000);
      const milliseconds = Math.floor(duration % 1000);
      console.log(`\nDoba trvání: ${minutes}m ${seconds}s ${milliseconds}ms\n`);
    }
  } as T;
};
