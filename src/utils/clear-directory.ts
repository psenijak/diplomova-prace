import fs from 'fs';
import path from 'path';

/**
 * Asynchronously clears all files in a specified directory.
 *
 * This function reads the contents of the specified directory and deletes each file found.
 * It uses Node.js's `fs` module to handle file operations asynchronously.
 *
 * @param {string} directoryPath - The path to the directory whose files should be deleted.
 * @returns {Promise<void>} - A promise that resolves when all files have been successfully deleted,
 * or rejects if an error occurs during the operation.
 *
 * Usage:
 * The function is designed to be called with the full path of the directory you wish to clear.
 * It will log a success message upon clearing all files, or an error message if the operation fails.
 *
 * Note:
 * - The function does not delete the directory itself, only its contents.
 * - Ensure that the directory path provided does not end with a slash ('/') for consistent behavior across platforms.
 * - This function does not handle the deletion of subdirectories and their contents. It only deletes files directly within the specified directory.
 */
export const clearDirectory = async (directoryPath: string): Promise<void> => {
  try {
    const files = await fs.promises.readdir(directoryPath);
    for (const file of files) {
      const filePath = path.join(directoryPath, file);
      await fs.promises.unlink(filePath);
    }
    console.log(
      `All files in the directory ${directoryPath} have been deleted.`,
    );
  } catch (error) {
    console.error(
      `An error occurred while deleting files in the directory ${directoryPath}:`,
      error,
    );
  }
};
