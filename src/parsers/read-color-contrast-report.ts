import fs from 'fs';
import { storeMetricsForCity } from '../utils/city-repository';

/**
 * Stores the number of color contrast issues from the given file data for a specific city.
 *
 * @param {number} cityId - The unique identifier for the city.
 * @param {any} fileData - Data containing color contrast issues.
 * @returns {Promise<void>} - A promise that resolves after storing the data.
 */
const storeColorContrastForCity = async (
  cityId: number,
  fileData: any,
): Promise<void> => {
  const numNodes = fileData[0]?.nodes?.length ?? 0;

  if (numNodes && typeof numNodes === 'number') {
    await storeMetricsForCity(cityId, {
      contrastProblems: numNodes,
    });
  }
};

/**
 * Loads and processes a JSON file with color contrast data, storing the results for a city.
 *
 * @param {string} filename - The file name containing color contrast data.
 * @param {number} cityId - The city ID to which the data pertains.
 * @returns {Promise<void>} - A promise that resolves when the data has been processed.
 *
 * Reads the specified file and analyzes the color contrast data contained within. If the file
 * is missing, inaccessible, or empty, it logs the issue and stores a default value indicating
 * no contrast problems were found.
 */
export const loadAndAnalyzeColorContrast = async (
  filename: string,
  cityId: number,
): Promise<void> => {
  try {
    const filePath = `./data/${filename}`;

    try {
      await fs.promises.access(filePath, fs.constants.F_OK);
    } catch {
      await storeMetricsForCity(cityId, {
        contrastProblems: 0,
      });
      return;
    }

    const data = await fs.promises.readFile(filePath, 'utf8');

    if (data.length === 0) {
      console.log(`Soubor ${filePath} je prázdný.`);
      return;
    }

    const json = JSON.parse(data);

    await storeColorContrastForCity(cityId, json);
  } catch (error) {
    await storeMetricsForCity(cityId, {
      contrastProblems: 0,
    });
    console.error(
      'Došlo k chybě při načítání nebo analýze JSON souboru:',
      error,
    );
  }
};
