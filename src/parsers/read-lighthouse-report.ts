import fs from 'fs';
import { storeMetricsForCity } from '../utils/city-repository';

/**
 * Stores SEO score for a city based on the Lighthouse report data.
 *
 * @param {number} cityId - The unique identifier of the city.
 * @param {any} fileData - The JSON parsed data from the Lighthouse report.
 * @returns {Promise<void>} - A promise that resolves after the SEO score is stored.
 */
const storeSeoCriteriaForCity = async (
  cityId: number,
  fileData: any,
): Promise<void> => {
  const performanceScore = fileData?.lhr?.categories?.seo?.score;

  if (performanceScore && typeof performanceScore === 'number') {
    await storeMetricsForCity(cityId, {
      seoScore: performanceScore * 100,
    });
  }
};

/**
 * Stores the performance score for a city based on the Lighthouse report data.
 *
 * @param {number} cityId - The unique identifier of the city.
 * @param {any} fileData - The JSON parsed data from the Lighthouse report.
 * @returns {Promise<void>} - A promise that resolves after the performance score is stored.
 */
const storePerformanceForCity = async (
  cityId: number,
  fileData: any,
): Promise<void> => {
  const performanceScore = fileData?.lhr?.categories?.performance?.score;

  if (performanceScore && typeof performanceScore === 'number') {
    await storeMetricsForCity(cityId, {
      loadingPerformance: performanceScore * 100,
    });
  }
};

/**
 * Evaluates and stores whether a city's website has proper ARIA attributes based on the Lighthouse report.
 *
 * @param {number} cityId - The unique identifier of the city.
 * @param {any} fileData - The JSON parsed data from the Lighthouse report.
 * @returns {Promise<void>} - A promise that resolves after the ARIA compliance check is completed and stored.
 */
const storeARIACriteriaForCity = async (
  cityId: number,
  fileData: any,
): Promise<void> => {
  const audits = fileData?.lhr?.audits;
  if (!audits) {
    console.error('Audits data is not available');
    return;
  }

  let hasARIAAttributes = true;
  for (const key of Object.keys(audits)) {
    if (key.startsWith('aria-')) {
      const score = audits[key].score;
      if (score === 0) {
        hasARIAAttributes = false;
        break;
      } else if (score === null) {
        continue;
      }
    }
  }

  await storeMetricsForCity(cityId, {
    hasARIAAttributes: hasARIAAttributes,
  });
};

/**
 * Loads and analyzes a Lighthouse JSON report from a specified file and stores relevant metrics for a city.
 *
 * @param {string} filename - The file containing the Lighthouse report.
 * @param {number} cityId - The ID of the city for which metrics are analyzed.
 * @returns {Promise<void>} - A promise that resolves after the Lighthouse data is analyzed and metrics are stored.
 */
export const loadAndAnalyzeLighthouse = async (
  filename: string,
  cityId: number,
): Promise<void> => {
  try {
    const filePath = `./data/${filename}`;

    const data = await fs.promises.readFile(filePath, 'utf8');

    if (data.length === 0) {
      return;
    }

    const json = JSON.parse(data);

    // store metrics
    await storeSeoCriteriaForCity(cityId, json);
    await storeARIACriteriaForCity(cityId, json);
    await storePerformanceForCity(cityId, json);
  } catch (error) {
    console.error(
      'Došlo k chybě při načítání nebo analýze JSON souboru:',
      error,
    );
  }
};
