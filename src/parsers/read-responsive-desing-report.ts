import fs from 'fs';
import { RESPONSIVE_DESIGN_DEVIATION } from '../types/constants';
import { storeMetricsForCity } from '../utils/city-repository';

interface LighthouseMetrics {
  seo: number;
  performance: number;
  accessibility: number;
  pwa: number;
}

/**
 * Analyzes multiple Lighthouse reports to determine if there is a significant deviation in any
 * web performance metrics across different devices or conditions.
 *
 * This function reads and parses several Lighthouse report files, extracts core web performance metrics,
 * and checks for significant deviations between them. If any metric deviates by more than 40%, it flags
 * the city's responsive design as inconsistent.
 *
 * @param {string[]} filenames - An array of filenames for the Lighthouse reports.
 * @param {number} cityId - The ID of the city for which the analysis is performed.
 * @returns {Promise<void>} - A promise that resolves once the analysis is complete and the results are stored.
 *
 * Exceptions:
 * - Logs an error and exits the function if any file is empty or an error occurs during file reading or parsing.
 */
export const analyzeResponsiveDesignReports = async (
  filenames: string[],
  cityId: number,
): Promise<void> => {
  const reports: LighthouseMetrics[] = [];

  for (const filename of filenames) {
    const filePath = `./data/${filename}`;
    try {
      const data = await fs.promises.readFile(filePath, 'utf8');
      if (data.length === 0) {
        throw new Error(`Soubor ${filePath} je prázdný.`);
      }
      const json = JSON.parse(data);
      const metrics = json?.lhr?.categories;
      if (
        metrics &&
        metrics.seo.score &&
        metrics.performance.score &&
        metrics.accessibility.score &&
        metrics.pwa.score
      ) {
        reports.push({
          seo: metrics.seo.score * 100,
          performance: metrics.performance.score * 100,
          accessibility: metrics.accessibility.score * 100,
          pwa: metrics.pwa.score * 100,
        });
      }
    } catch (error) {
      console.error('Chyba při načítání souboru:', error);
      return;
    }
  }

  // Analyze for significant deviations
  if (reports.length > 1) {
    const analyzeDeviation = (metric: keyof LighthouseMetrics) => {
      const values = reports.map((report) => report[metric]);
      const max = Math.max(...values);
      const min = Math.min(...values);
      return (max - min) / Math.max(min, 1) > RESPONSIVE_DESIGN_DEVIATION; // Checks for over 40% deviation
    };

    const significantDeviationExists = [
      'seo',
      'performance',
      'accessibility',
      'pwa',
    ].some((metric) => analyzeDeviation(metric as keyof LighthouseMetrics));

    await storeMetricsForCity(cityId, {
      hasResponsiveDesign: significantDeviationExists,
    });
  }
};
