import fs from 'fs';
import { storeMetricsForCity } from '../utils/city-repository';

/**
 * Stores WCAG compliance issues for a specific city based on data parsed from a JSON file.
 *
 * @param {number} cityId - The unique identifier for the city in the database.
 * @param {any} fileData - Data containing WCAG compliance issues.
 * @returns {Promise<void>} - A promise that resolves after the WCAG issues are stored.
 *
 * This function calculates the number of WCAG issues at various severity levels (minor, moderate,
 * serious, critical) and updates these metrics in the city's database record.
 */
const storeWCAGForCity = async (
  cityId: number,
  fileData: any,
): Promise<void> => {
  const impactCounts: Record<string, number> = {
    minor: 0,
    moderate: 0,
    serious: 0,
    critical: 0,
  };

  fileData.forEach((rule) => {
    rule.nodes.forEach((node) => {
      if (node.impact) {
        const impactLevel = node.impact.toLowerCase();
        if (impactCounts.hasOwnProperty(impactLevel)) {
          impactCounts[impactLevel]++;
        }
      }
    });
  });

  await storeMetricsForCity(cityId, {
    WCAGCriticalProblemCount: impactCounts.critical,
    WCAGMinorProblemCount: impactCounts.minor,
    WCAGModerateProblemCount: impactCounts.moderate,
    WCAGSeriousProblemCount: impactCounts.serious,
  });
};

/**
 * Loads a WCAG compliance report from a specified file and analyzes it to store compliance metrics.
 *
 * @param {string} filename - The name of the file containing the WCAG report.
 * @param {number} cityId - The ID of the city for which the WCAG data is to be analyzed.
 * @returns {Promise<void>} - A promise that resolves when the analysis and data storage are complete.
 *
 * This function handles file access, data parsing, and subsequent storage of WCAG compliance metrics.
 * If the file is inaccessible or empty, it defaults the WCAG problem counts to zero.
 */
export const loadAndAnalyzeWCAGReport = async (
  filename: string,
  cityId: number,
): Promise<void> => {
  try {
    const filePath = `./data/${filename}`;

    try {
      await fs.promises.access(filePath, fs.constants.F_OK);
    } catch {
      await storeMetricsForCity(cityId, {
        WCAGCriticalProblemCount: 0,
        WCAGMinorProblemCount: 0,
        WCAGModerateProblemCount: 0,
        WCAGSeriousProblemCount: 0,
      });
      return;
    }

    const data = await fs.promises.readFile(filePath, 'utf8');

    if (data.length === 0) {
      console.log(`Soubor ${filePath} je prázdný.`);
      return;
    }

    const json = JSON.parse(data);

    await storeWCAGForCity(cityId, json);
  } catch (error) {
    await storeMetricsForCity(cityId, {
      WCAGCriticalProblemCount: 0,
      WCAGMinorProblemCount: 0,
      WCAGModerateProblemCount: 0,
      WCAGSeriousProblemCount: 0,
    });
    console.error(
      'Došlo k chybě při načítání nebo analýze JSON souboru:',
      error,
    );
  }
};
