import { City, PrismaClient, UsageType } from '@prisma/client';

let prisma: PrismaClient;

if (prisma === undefined) {
  prisma = new PrismaClient();
}

/**
 * Selects a random subset of IDs from an array.
 * This function randomly selects a specified number of IDs from a given array without replacement.
 *
 * @param {number[]} ids - An array of city IDs from which to select.
 * @param {number} setSize - The number of IDs to select.
 * @returns {number[]} An array containing the selected IDs.
 */
const selectRandomIds = (ids: number[], setSize: number) => {
  const selectedIds = [];
  const idPool = [...ids];

  while (selectedIds.length < setSize && idPool.length > 0) {
    const randomIndex = Math.floor(Math.random() * idPool.length);
    selectedIds.push(idPool[randomIndex]);
    idPool.splice(randomIndex, 1);
  }

  return selectedIds;
};

/**
 * Adds a new usage type to an array of existing usage types, ensuring no duplicates.
 *
 * @param {UsageType[]} existingTypes - Array of existing usage types.
 * @param {UsageType} newType - The new usage type to add.
 * @returns {UsageType[]} An array containing the updated list of usage types.
 */
const addUsageType = (
  existingTypes: UsageType[],
  newType: UsageType,
): UsageType[] => {
  const typesSet = new Set(existingTypes);
  typesSet.add(newType);
  return Array.from(typesSet);
};

/**
 * Updates the usage type of a list of cities in the database.
 *
 * @param {City[]} cities - Array of city objects to update.
 * @param {UsageType} newType - The new usage type to assign to each city.
 * @returns {Promise<void>} A promise that resolves when all updates are completed.
 */
const updateCitiesSet = async (
  cities: City[],
  newType: UsageType,
): Promise<void> => {
  const updates = cities.map(async (city) => {
    const currentTypes = city.usageType ? city.usageType : [];
    const updatedTypes = addUsageType(currentTypes, newType);

    return prisma.city.update({
      where: { id: city.id },
      data: { usageType: updatedTypes },
    });
  });

  await Promise.all(updates);
};

/**
 * Assigns training and testing sets to cities without existing training or testing designation.
 * The function ensures no city is double-assigned by first checking existing assignments.
 *
 * @param {number} setSize - Total number of cities to include in sets.
 * @param {number} trainingRationCount - Number of cities to include in the training set.
 * @param {boolean} checkAlreadySet - Whether to check if any city already has a set assignment.
 * @throws {Error} Throws an error if there are cities already set or if there are not enough cities available.
 * @returns {Promise<void>} A promise that resolves when both sets are successfully created.
 */
export const setTestingAndTrainingSet = async (
  setSize: number,
  trainingRationCount: number,
  checkAlreadySet: boolean = true,
): Promise<void> => {
  if (setSize < trainingRationCount) {
    console.error('Size of set is smaller than training ration count.');
    return;
  }

  if (checkAlreadySet) {
    const citiesWithSetCount = await prisma.city.count({
      where: {
        OR: [
          { usageType: { has: UsageType.testing } },
          { usageType: { has: UsageType.training } },
        ],
      },
    });
    console.log(citiesWithSetCount);
    if (citiesWithSetCount > 0) {
      console.error('Some cities already have set.');
      return;
    }
  }
  const cityIds = await prisma.city.findMany({
    where: {
      OR: [
        {
          NOT: {
            OR: [
              { usageType: { has: UsageType.testing } },
              { usageType: { has: UsageType.training } },
            ],
          },
        },
        { usageType: { equals: null } },
      ],
    },
    select: { id: true },
  });

  if (cityIds.length < setSize) {
    console.error('Size of set is bigger than cities without set count.');
    return;
  }

  const selectedIds = selectRandomIds(
    cityIds.map((city) => city.id),
    setSize,
  );

  const foundCities = await prisma.city.findMany({
    where: {
      id: { in: selectedIds },
    },
  });

  const trainingSet = foundCities.slice(0, trainingRationCount);
  const testingSet = foundCities.slice(trainingRationCount);

  await updateCitiesSet(trainingSet, UsageType.training);
  await updateCitiesSet(testingSet, UsageType.training);
  console.log('Sets were created.');
};
