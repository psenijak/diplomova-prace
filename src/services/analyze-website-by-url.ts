import cypress from 'cypress';
import { loadAndAnalyzeColorContrast } from '../parsers/read-color-contrast-report';
import { loadAndAnalyzeLighthouse } from '../parsers/read-lighthouse-report';
import { analyzeResponsiveDesignReports } from '../parsers/read-responsive-desing-report';
import { loadAndAnalyzeWCAGReport } from '../parsers/read-wcag-report';
import {
  getCityById,
  resetCityMetrics,
  storeMetricsForCity,
  verifyCityExistenceByUrl,
} from '../utils/city-repository';
import { clearDirectory } from '../utils/clear-directory';

/**
 * Runs a series of web testing and analysis operations on a specified city URL using Cypress and various reporting tools.
 *
 * This function first verifies the existence of a city by its URL, and if found, it resets previous metrics,
 * clears the relevant directory, and then runs several tests including Lighthouse and accessibility checks.
 * The results from these tests are then analyzed and stored in the database.
 *
 * The function handles errors during the testing process and logs appropriate error messages to the console.
 *
 * @param {string} url - The URL of the city to be analyzed. This URL is used to locate the city in the database and as the base URL for Cypress tests.
 * @returns {Promise<void>} A promise that resolves when all operations are complete. If an error occurs, it is caught and logged.
 *
 * Errors:
 * - If the city is not found by URL, logs an error and exits.
 * - If the city does not have a properly defined URL, logs an error and exits.
 * - Errors from test execution or data processing are caught and logged.
 *
 * Usage:
 * This function should be invoked with the URL of a city as an argument. It is designed to be used within a larger application
 * that handles scheduling and invocation of testing tasks.
 */
export const runCypressForUrl = async (url: string): Promise<void> => {
  let city = await verifyCityExistenceByUrl(url);

  if (!city) {
    console.error(`Město s URL ${url} nebylo nalezeno.`);
    return;
  }

  if (!city.website_url) {
    console.error(`Město s ID ${city.id} nemá uvedenou URL.`);
    return;
  }

  await resetCityMetrics(city.id);
  await clearDirectory('./data');
  city = await getCityById(city.id);

  try {
    // run analyze test
    await cypress.run({
      spec: 'cypress/e2e/analyze-city-website.cy.ts',
      browser: 'chrome',
      headless: true,
      quiet: true,
      config: {
        env: {
          baseUrl: city.website_url,
        },
      },
    });
    // run lighthouse test
    await cypress.run({
      spec: 'cypress/e2e/lighthouse.cy.ts',
      browser: 'chrome',
      headless: true,
      quiet: true,
      config: {
        env: {
          baseUrl: city.website_url,
          device: 'PC',
        },
      },
    });
    await cypress.run({
      spec: 'cypress/e2e/lighthouse.cy.ts',
      browser: 'chrome',
      headless: true,
      quiet: true,
      config: {
        env: {
          baseUrl: city.website_url,
          device: 'MOBILE',
        },
      },
    });
    await cypress.run({
      spec: 'cypress/e2e/lighthouse.cy.ts',
      browser: 'chrome',
      headless: true,
      quiet: true,
      config: {
        env: {
          baseUrl: city.website_url,
          device: 'TABLET',
        },
      },
    });

    await loadAndAnalyzeLighthouse('lighthouse-report-pc.json', city.id);
    await loadAndAnalyzeWCAGReport('a11y-report.json', city.id);
    await loadAndAnalyzeColorContrast(
      'a11y-report-color-contrast.json',
      city.id,
    );
    await analyzeResponsiveDesignReports(
      [
        'lighthouse-report-pc.json',
        'lighthouse-report-tablet.json',
        'lighthouse-report-mobile.json',
      ],
      city.id,
    );

    await storeMetricsForCity(city.id, {
      lastCrawledAt: new Date(),
    });
    console.log(`Testy pro ${url} byly dokončeny.`);
  } catch (error) {
    console.error(`Chyba při spuštění testů pro ${url}:`, error);
  }
};
