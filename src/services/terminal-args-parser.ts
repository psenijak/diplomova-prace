import yargs from 'yargs';
import { hideBin } from 'yargs/helpers';

export interface CommandArgs {
  url?: string; // URL of the page for analysis, optional
  print?: boolean; // Print the analysis results on the console, optional
  set?: 'tr' | 'ts' | 'i'; // Selection of the set for analysis, optional
  load?: boolean; // Load websites into the database, optional
  setSize?: number; // Size of the set if defined, optional
  trainingCount?: number; // Size of the training set, defines the split ratio between training and testing sets, optional
  checkSets?: boolean; // Check if the set is already defined and can be overwritten, irreversible change! Set to false if overwriting is to be avoided, optional
  defineSets?: boolean; // Switch for defining training and testing sets, optional
}

/**
 * Parses the command line arguments using yargs.
 *
 * This function initializes command line options and handles conditional checks
 * to ensure the required dependencies between options are met. It leverages the
 * `yargs` library to define options and enforce rules.
 *
 * @returns {CommandArgs} An object containing all parsed options with their values.
 *
 * Options:
 *  - `url` (string): URL of the page to analyze.
 *  - `print` (boolean): Whether to print the analysis results on the console.
 *  - `set` (string): Selection of the set for analysis, with choices ['tr', 'ts', 'i'].
 *  - `load` (boolean): Whether to load websites into the database.
 *  - `setSize` (number): Size of the set if defined, used to specify how many items in the set.
 *  - `trainingCount` (number): Size of the training set, used to determine the split ratio
 *    between training and testing sets.
 *  - `checkSets` (boolean): Whether to allow overwriting of already defined sets.
 *    This change is irreversible. Set to false if overwriting should be avoided.
 *
 * Throws:
 *  - Error if the `defineSets` option is set and `setSize` or `trainingCount` is not specified.
 *
 * The function uses `strict()` mode to ensure that no unconfigured options are passed
 * and applies custom checks with `.check()` to validate dependencies among options.
 */
export const parseArgs = (): CommandArgs => {
  const argv = yargs(hideBin(process.argv))
    .option('url', {
      alias: 'u',
      type: 'string',
      description: 'URL adresa stránky, kterou chcete analyzovat',
    })
    .option('print', {
      alias: 'p',
      type: 'boolean',
      description: 'Vypsat výsledek analýzy na konzoli bez analýzy',
    })
    .option('set', {
      alias: 's',
      type: 'string',
      choices: ['tr', 'ts', 'i'],
      description: 'Výběr celé sady pro analýzu',
    })
    .option('load', {
      alias: 'l',
      type: 'boolean',
      description: 'Načtení webů do databáze',
    })
    .option('setSize', {
      alias: 'z',
      type: 'number',
      description: 'Velikost sady, pokud je definována',
    })
    .option('trainingCount', {
      alias: 'r',
      type: 'number',
      description:
        'Velikost trénovací sady, udává následně poměr rozdělení trénovací a testovací sady',
    })
    .option('checkSets', {
      alias: 'c',
      type: 'boolean',
      description:
        'Pokud je množina již definována, může být přepsána, znovu. Změna je nevratná! Nastavte na false, pokud má být přepsáno.',
    })
    .conflicts('u', ['s', 'l', 'z'])
    .conflicts('s', ['u', 'l', 'z'])
    .conflicts('l', ['u', 's', 'z'])
    .conflicts('z', ['u', 's', 'l'])
    .help()
    .alias('help', 'h')
    .strict().argv;

  return argv as CommandArgs;
};
