import cypress from 'cypress';

/**
 * Runs Cypress tests for specified URLs.
 *
 * This function orchestrates a sequence of Cypress test runs to automate the process of:
 * 1. Iterating over wiki tables to store each city's Wikipedia page.
 * 2. Storing each city's official website URL into the database.
 *
 * The function handles these operations using headless Chrome, minimizing the output to keep the process quiet.
 *
 * @throws {Error} Captures and logs any errors encountered during the test runs.
 *
 * Usage:
 * This function is designed to be called without parameters and will execute predefined Cypress tests.
 * These tests should be specified in the 'spec' properties within the Cypress run configuration.
 *
 * Example tests included:
 * - 'list-of-wiki-pages.cy.ts' to scrape Wikipedia for city data.
 * - 'fetch-city-web-url.cy.ts' to extract and store city website URLs.
 *
 * Each test run is performed in a headless Chrome browser, suitable for automated testing environments.
 *
 * Logging:
 * - On successful completion, prints a message indicating that city data loading is complete.
 * - On error, prints a detailed error message to the console.
 */
export const loadCitiesWebsiteAndStoreToDb = async (): Promise<void> => {
  try {
    // iterate over wikitables and store city's wiki page
    await cypress.run({
      spec: 'cypress/e2e/list-of-wiki-pages.cy.ts',
      browser: 'chrome',
      headless: true,
      quiet: true,
    });
    // store city's website for each row in database
    await cypress.run({
      spec: 'cypress/e2e/fetch-city-web-url.cy.ts',
      browser: 'chrome',
      headless: true,
      quiet: true,
    });
    console.log(`Načítání měst do databáze dokončeno.`);
  } catch (error) {
    console.error(`Chyba při načítání webů měst do databáze: `, error);
  }
};
