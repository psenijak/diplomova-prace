import clc from 'cli-color';
import Table from 'cli-table3';
import { CityWithRelations } from '../types/city-with-relations';
import { getCityByUrlWithRelations } from '../utils/city-repository';

type MetricRow = {
  name: string;
  value: string;
  note?: string;
};

/**
 * Handles the generation and printing of reports for city data, encapsulating detailed metrics.
 *
 * This class fetches and processes detailed city data including SEO, accessibility, performance,
 * and web presence metrics. It provides functionality to load city data from a specified URL,
 * format the data, and then print it out in a table format to the console.
 *
 * Usage:
 * 1. Create an instance with a city URL.
 * 2. Call `printReportToConsole` to load data, process it, and print the report.
 *
 * Methods:
 * - loadRelationsForCity: Loads city data including related metrics from the database.
 * - printReportToConsole: Fetches data, prepares it, and prints it to the console.
 * - wrapText: Wraps text to fit within a specified column width for better display in the console.
 */
export class ReportPrinter {
  private city: CityWithRelations;

  /**
   * Constructor to create a report printer instance.
   * @param {string} cityUrl - URL of the city to generate the report for.
   */
  constructor(private readonly cityUrl: string) {}

  /**
   * Private method to wrap text into multiple lines.
   * @param {string | undefined} text - The text to wrap.
   * @param {number} maxLineWidth - Maximum width of the line.
   * @returns {string} The wrapped text.
   */
  private wrapText(text: string | undefined, maxLineWidth: number): string {
    const words = text.split(' ');
    const lines = [];
    let currentLine = words[0];

    for (let i = 1; i < words.length; i++) {
      if (currentLine.length + words[i].length + 1 <= maxLineWidth) {
        currentLine += ' ' + words[i];
      } else {
        lines.push(currentLine);
        currentLine = words[i];
      }
    }
    lines.push(currentLine);
    return lines.join('\n');
  }

  /**
   * Private method to load city data and its relations.
   * Logs errors if the city is not found or has not been tested.
   * @returns {Promise<void>}
   */
  private async loadRelationsForCity(): Promise<boolean> {
    const city = await getCityByUrlWithRelations(this.cityUrl);

    if (!city) {
      console.error(`Město s URL ${this.cityUrl} nebylo nalezeno.`);
      return false;
    }
    if (!city.lastCrawledAt) {
      console.error(`Město s URL ${this.cityUrl} nebylo testováno.`);
      return false;
    }
    this.city = city;
    return true;
  }

  /**
   * Public method to print the full report to the console.
   * This method handles the orchestration of data loading, processing, and printing.
   * @returns {Promise<void>}
   */
  async printReportToConsole(): Promise<void> {
    const res = await this.loadRelationsForCity();
    if (!res) {
      return;
    }

    const headerData = {
      'Název města': this.city.name,
      Okres: this.city.district?.name,
      Kraj: this.city.district?.region?.name,
      'Počet obyvatel': this.city.citizens,
      'Datum posledního dolování reportu':
        this.city.lastCrawledAt?.toLocaleString(),
    };

    const headerTable = new Table({
      style: { head: [], border: [] },
      colWidths: [40, 30],
    });

    headerTable.push(
      ...Object.entries(headerData).map(([key, value]) => [
        clc.bold(key),
        value,
      ]),
    );

    const terminalWidth = process.stdout.columns || 120;
    const colWidthFirst = 30;
    const colWidthSecond = 25;
    const colWidthThird = terminalWidth - (colWidthFirst + colWidthSecond) - 5;
    const metricTable = new Table({
      head: [
        clc.blue('Název metriky'),
        clc.blue('Hodnota'),
        clc.blue('Poznámka'),
      ],
      style: { head: [], border: [] },
      colWidths: [colWidthFirst, colWidthSecond, colWidthThird],
    });

    const metrics: MetricRow[] = [
      this.getCorrectLinks(),
      this.getAltTexts(),
      this.getCorrectARIAAttributes(),
      this.getBreadcrumbs(),
      this.getContactForm(),
      this.getValidForms(),
      this.getHierarchyOfHeadings(),
      this.getCorrectSemanticLinks(),
      this.getCalendar(),
      this.getHTTPS(),
      this.getOfficialBoard(),
      this.getResponsiveDesign(),
      this.getHasSecuredCookies(),
      this.getHasSocialNetworks(),
      this.getHasWorkingHours(),
      this.getLoadingPerformance(),
      this.getPrivacyPolicy(),
      this.getCookieConsent(),
      this.getColorContrastProblems(),
      this.getEmailOnMainPage(),
      this.getPhoneOnMainPage(),
      this.getRobots(),
      this.getSitemap(),
      this.getSeoScore(),
      this.getWCAGProblems(),
    ];

    metrics.forEach((metric) => {
      metricTable.push([
        clc.blue(metric.name),
        metric.value,
        metric.note && this.wrapText(metric.note, colWidthThird),
      ]);
    });
    console.log(headerTable.toString());
    console.log(metricTable.toString());
  }

  private checkValueIfDefined(
    value: string,
    originalValue: number | boolean | string,
  ): string {
    return originalValue === null || originalValue === undefined
      ? clc.bgYellowBright(' Nezjištěno ')
      : value;
  }

  private getCorrectLinks(): MetricRow {
    const value =
      this.city.contactLinks?.length > 0 ? clc.green('Ano') : clc.red('Ne');
    return {
      name: 'Kontaktní stránka',
      value: value,
      note: 'Přítomnost kontaktní stránky na webu.',
    };
  }

  private getAltTexts(): MetricRow {
    const value = this.checkValueIfDefined(
      this.city.hasALTAttributes ? clc.green('Ano') : clc.red('Ne'),
      this.city.hasALTAttributes,
    );
    return {
      name: 'Alternativní texty',
      value: value,
      note: 'Přítomnost alternativních textů u obrázků (musí mít hodnotu, ne pouze atribut nebo prázdný řetězec).',
    };
  }

  private getCorrectARIAAttributes(): MetricRow {
    const value = this.checkValueIfDefined(
      this.city.hasARIAAttributes ? clc.green('Ano') : clc.red('Ne'),
      this.city.hasARIAAttributes,
    );
    return {
      name: 'Přítomnost ARIA atributů',
      value: value,
      note: 'Přítomnost atributů pro zlepšení přístupnosti.',
    };
  }

  private getBreadcrumbs(): MetricRow {
    const value = this.checkValueIfDefined(
      this.city.hasBreadcrumbs ? clc.green('Ano') : clc.red('Ne'),
      this.city.hasBreadcrumbs,
    );
    return {
      name: 'Přítomnost BreadCrumbs',
      value: value,
      note: 'Přítomnost drobečkové navigace.',
    };
  }

  private getContactForm(): MetricRow {
    const value = this.checkValueIfDefined(
      this.city.hasContactForm ? clc.green('Ano') : clc.red('Ne'),
      this.city.hasContactForm,
    );
    return {
      name: 'Přítomnost Kontaktního formuláře',
      value: value,
      note: 'Přítomnost formuláře pro dotazy a zpětnou vazbu, ne pouze e-mailová adresa.',
    };
  }

  private getValidForms(): MetricRow {
    const value = this.checkValueIfDefined(
      this.city.hasCorrectForms ? clc.green('Ano') : clc.red('Ne'),
      this.city.hasCorrectForms,
    );
    return {
      name: 'Správnost formulářů',
      value: value,
      note: 'Zda jsou formuláře validní a přístupné.',
    };
  }

  private getHierarchyOfHeadings(): MetricRow {
    const value = this.checkValueIfDefined(
      this.city.hasCorrectHierarchyOfHeadings
        ? clc.green('Ano')
        : clc.red('Ne'),
      this.city.hasCorrectHierarchyOfHeadings,
    );
    return {
      name: 'Správnost hierarchie nadpisů',
      value: value,
      note: 'Zda je dodržena správná hierarchie nadpisů.',
    };
  }

  private getCorrectSemanticLinks(): MetricRow {
    const value = this.checkValueIfDefined(
      this.city.hasCorrectSemanticLinks ? clc.green('Ano') : clc.red('Ne'),
      this.city.hasCorrectSemanticLinks,
    );
    return {
      name: 'Sémantická správnost odkazů',
      value: value,
      note: 'Správné použití sémantických odkazů ( mailto:, tel: ).',
    };
  }

  private getCalendar(): MetricRow {
    const value = this.checkValueIfDefined(
      this.city.hasEventCalendar ? clc.green('Ano') : clc.red('Ne'),
      this.city.hasEventCalendar,
    );
    return {
      name: 'Kalendář událostí',
      value: value,
      note: 'Přítomnost kalendáře událostí na webu.',
    };
  }

  private getHTTPS(): MetricRow {
    const value = this.checkValueIfDefined(
      this.city.hasHTTPS ? clc.green('Ano') : clc.red('Ne'),
      this.city.hasHTTPS,
    );
    return {
      name: 'HTTPS',
      value: value,
      note: 'Zda je web dostupný přes zabezpečené spojení a vynucuje ho.',
    };
  }

  private getOfficialBoard(): MetricRow {
    const value = this.checkValueIfDefined(
      this.city.hasOfficialBoard ? clc.green('Ano') : clc.red('Ne'),
      this.city.hasOfficialBoard,
    );
    return {
      name: 'Úřední deska',
      value: value,
      note: 'Přítomnost úřední desky na webu.',
    };
  }

  private getResponsiveDesign(): MetricRow {
    const value = this.checkValueIfDefined(
      this.city.hasResponsiveDesign ? clc.green('Ano') : clc.red('Ne'),
      this.city.hasResponsiveDesign,
    );
    return {
      name: 'Responzivní design',
      value: value,
      note: 'Zda je web optimalizován pro různé typy zařízení.',
    };
  }

  private getHasSecuredCookies(): MetricRow {
    const value = this.checkValueIfDefined(
      this.city.hasSecuredCookies ? clc.green('Ano') : clc.red('Ne'),
      this.city.hasSecuredCookies,
    );
    return {
      name: 'Zabezpečené cookies',
      value: value,
      note: 'Zda jsou cookies zabezpečeny proti útokům.',
    };
  }

  private getHasSocialNetworks(): MetricRow {
    const value = this.checkValueIfDefined(
      this.city.hasSocialNetworks ? clc.green('Ano') : clc.red('Ne'),
      this.city.hasSocialNetworks,
    );
    return {
      name: 'Sociální sítě',
      value: value,
      note: 'Zda jsou na webu přítomny odkazy na sociální sítě obce.',
    };
  }

  private getHasWorkingHours(): MetricRow {
    const value = this.checkValueIfDefined(
      this.city.hasWorkingHours ? clc.green('Ano') : clc.red('Ne'),
      this.city.hasWorkingHours,
    );
    return {
      name: 'Úřední hodiny',
      value: value,
      note: 'Přítomnost úředních hodin na webu.',
    };
  }

  private getLoadingPerformance(): MetricRow {
    const value = this.checkValueIfDefined(
      this.city.loadingPerformance > 70
        ? clc.green('Vynikající')
        : this.city.loadingPerformance > 45
          ? clc.yellow('Dostačující')
          : clc.red('Nedostačující'),
      this.city.loadingPerformance,
    );
    return {
      name: 'Rychlost načítání',
      value: `${value} (${this.city.loadingPerformance}/100)`,
      note: 'Rychlost načítání webu a jeho částí - správná optimalizace obrázků, kodu, stylů, scriptů, atd.',
    };
  }

  private getPrivacyPolicy(): MetricRow {
    const value = this.checkValueIfDefined(
      this.city.providesPrivacyPolicy ? clc.green('Ano') : clc.red('Ne'),
      this.city.providesPrivacyPolicy,
    );
    return {
      name: 'Ochrana osobních údajů',
      value: value,
      note: 'Přítomnost zásad ochrany osobních údajů na webu.',
    };
  }

  private getCookieConsent(): MetricRow {
    const value = this.checkValueIfDefined(
      this.city.respectsCookiesConsent ? clc.green('Ano') : clc.red('Ne'),
      this.city.respectsCookiesConsent,
    );
    return {
      name: 'Souhlas s použitím cookies',
      value: value,
      note: 'Respektování souhlasu s použitím cookies - neodesílání analytických GA bez souhlasu.',
    };
  }

  private getColorContrastProblems(): MetricRow {
    const value = this.checkValueIfDefined(
      this.city.contrastProblems === 0
        ? clc.green(this.city.contrastProblems)
        : this.city.contrastProblems < 10
          ? clc.yellow(this.city.contrastProblems)
          : clc.red(this.city.contrastProblems),
      this.city.contrastProblems,
    );
    return {
      name: 'Kontrast',
      value: value,
      note: 'Počet elementů s nedostatečným kontrastem na úrovni AA.',
    };
  }

  private getEmailOnMainPage(): MetricRow {
    const value = this.checkValueIfDefined(
      this.city.hasEmailsOnMainPage ? clc.green('Ano') : clc.red('Ne'),
      this.city.hasEmailsOnMainPage,
    );
    return {
      name: 'Email',
      value: value,
      note: 'Přítomnost e-mailových adres na hlavní stránce.',
    };
  }

  private getPhoneOnMainPage(): MetricRow {
    const value = this.checkValueIfDefined(
      this.city.hasPhoneNumbersOnMainPage ? clc.green('Ano') : clc.red('Ne'),
      this.city.hasPhoneNumbersOnMainPage,
    );
    return {
      name: 'Telefon',
      value: value,
      note: 'Přítomnost telefonních čísel na hlavní stránce.',
    };
  }

  private getRobots(): MetricRow {
    const value = this.checkValueIfDefined(
      this.city.hasRobots ? clc.green('Ano') : clc.red('Ne'),
      this.city.hasRobots,
    );
    return {
      name: 'Robots.txt',
      value: value,
      note: 'Přítomnost souboru robots.txt pro řízení indexace.',
    };
  }

  private getSitemap(): MetricRow {
    const value = this.checkValueIfDefined(
      this.city.hasSiteMap ? clc.green('Ano') : clc.red('Ne'),
      this.city.hasSiteMap,
    );
    return {
      name: 'SiteMap',
      value: value,
      note: 'Přítomnost souboru sitemap.xml pro indexaci ve správném formátu.',
    };
  }

  private getSeoScore(): MetricRow {
    const value = this.checkValueIfDefined(
      this.city.seoScore > 70
        ? clc.green('Vynikající')
        : this.city.seoScore > 45
          ? clc.yellow('Dostačující')
          : clc.red('Nedostačující'),
      this.city.seoScore,
    );
    return {
      name: 'SEO skóre',
      value: `${value} (${this.city.seoScore}/100)`,
      note: 'SEO skóre webu - kvalita obsahu, klíčová slova, meta tagy, atd.',
    };
  }

  private getWCAGProblems(): MetricRow {
    const valueMinor = this.checkValueIfDefined(
      this.city.WCAGMinorProblemCount === 0
        ? clc.green(this.city.WCAGMinorProblemCount)
        : this.city.WCAGMinorProblemCount < 10
          ? clc.yellow(this.city.WCAGMinorProblemCount)
          : clc.red(this.city.WCAGMinorProblemCount),
      this.city.WCAGMinorProblemCount,
    );

    const valueModerate = this.checkValueIfDefined(
      this.city.WCAGModerateProblemCount === 0
        ? clc.green(this.city.WCAGModerateProblemCount)
        : this.city.WCAGModerateProblemCount < 10
          ? clc.yellow(this.city.WCAGModerateProblemCount)
          : clc.red(this.city.WCAGModerateProblemCount),
      this.city.WCAGModerateProblemCount,
    );

    const valueSerious = this.checkValueIfDefined(
      this.city.WCAGSeriousProblemCount === 0
        ? clc.green(this.city.WCAGSeriousProblemCount)
        : this.city.WCAGSeriousProblemCount < 10
          ? clc.yellow(this.city.WCAGSeriousProblemCount)
          : clc.red(this.city.WCAGSeriousProblemCount),
      this.city.WCAGSeriousProblemCount,
    );

    const valueCritical = this.checkValueIfDefined(
      this.city.WCAGCriticalProblemCount === 0
        ? clc.green(this.city.WCAGCriticalProblemCount)
        : this.city.WCAGCriticalProblemCount < 10
          ? clc.yellow(this.city.WCAGCriticalProblemCount)
          : clc.red(this.city.WCAGCriticalProblemCount),
      this.city.WCAGCriticalProblemCount,
    );
    const value = `${valueMinor} Drobných \n${valueModerate} Mírných\n${valueSerious} Závažných\n${valueCritical} Kritických`;
    return {
      name: 'WCAG',
      value: value,
      note: 'Počet nalezených problémů dle WCAG 2.1 rozdělené dle závažnosti.',
    };
  }
}
