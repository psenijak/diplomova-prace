import { UsageType } from '@prisma/client';
import { runCypressForUrl } from './services/analyze-website-by-url';
import { setTestingAndTrainingSet } from './services/define-training-and-testing-set';
import { loadCitiesWebsiteAndStoreToDb } from './services/load-websites-from-wiki';
import { ReportPrinter } from './services/print-results-to-console';
import { CommandArgs, parseArgs } from './services/terminal-args-parser';
import { getCitiesSetByUsageType } from './utils/city-repository';
import { withTiming } from './utils/timer';

const iterateOverOneCity = async (
  website_url: string,
  print?: boolean,
): Promise<void> => {
  if (print === undefined) {
    await runCypressForUrl(website_url);
  }
  const reportPrinter = new ReportPrinter(website_url);
  await reportPrinter.printReportToConsole();
};

const timedIterateOverOneCity = withTiming(iterateOverOneCity);

/**
 * Entry point for the application which orchestrates various tasks based on command line arguments.
 *
 * The main function evaluates command line arguments provided by the user to decide
 * which action to perform. It can load city data from a wiki, set up training and testing sets,
 * or process individual URLs for city data analysis based on the usage type.
 *
 * Exceptions:
 * - Throws an error if no valid command line option is specified.
 *
 * @param {CommandArgs} args - Parsed command line arguments specifying operation modes and parameters.
 * @returns {Promise<void>} - The asynchronous nature of the main function allows for operations that involve network requests and heavy processing.
 */
const main = async (args: CommandArgs): Promise<void> => {
  if (args.load) {
    void loadCitiesWebsiteAndStoreToDb();
  } else if (args.setSize) {
    void setTestingAndTrainingSet(
      args.setSize ?? 30,
      args.trainingCount ?? 21,
      args.checkSets ?? true,
    );
  } else if (args.set) {
    const usageType =
      args.set === 'tr'
        ? UsageType.training
        : args.set === 'ts'
          ? UsageType.testing
          : UsageType.manual;

    const cities = await getCitiesSetByUsageType(usageType);
    for (const city of cities) {
      await timedIterateOverOneCity(city.website_url, args.print);
    }
  } else if (args.url) {
    await timedIterateOverOneCity(args.url, args.print);
  } else {
    throw new Error('Není definován žádný přepínač, pro nápovědu použijte -h');
  }
};

const args = parseArgs();
void main(args);
